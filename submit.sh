#!/bin/sh

#SBATCH --job-name=AnomVAE
#SBATCH --ntasks=1
#SBATCH --output=%A_%a.out
#SBATCH --cpus-per-task=4
#SBATCH --mem=16GB
#SBATCH --partition=shared-gpu,private-dpnc-gpu
#SBATCH --gres=gpu:1
#SBATCH --time=12:00:00

cd /home/users/l/leighm/graphnets

module load GCC/9.3.0 Singularity/3.7.3-GCC-9.3.0-Go-1.14

srun singularity exec --nv -B /home,/srv /home/users/l/leighm/Images/gnets-image \
        python train_vae.py \
        --name TransformerVAE_FSK_5e5_StrongLat \
        --save_dir /home/users/l/leighm/scratch/Saved_Networks/JetTag/WhitePaper/Unsupervised/ \
        --lr 5e-5 \
        --gen_loss_name modifiedsinkhorn \