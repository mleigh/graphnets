"""
Example plotting script
"""

from pathlib import Path
import numpy as np

from jutils.plotting import (
    plot_roc_curves,
    plot_score_distributions,
    plot_mass_distributions,
    plot_mass_sculpting,
    plot_mass_score_correlation,
)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


def main():
    """Main script"""

    output_dir = "plots/restricted"
    data_dir = Path("/srv/beegfs/scratch/groups/rodem/anomalous_jets/restricted_data/")
    net_dir = Path("/srv/beegfs/scratch/groups/rodem/anomalous_jets/exported_taggers/")
    networks = [
        {
            "path": net_dir / "CLR_max",
            "label": "Dense-CLR500",
            "score_name": "scores",
            "color": "r",
        },
        {
            "path": net_dir / "transformer_restricted",
            "label": "Transformer",
            "score_name": "score",
            "color": "g",
        },
        {
            "path": net_dir / "graph_restricted",
            "label": "GraphNet",
            "score_name": "score",
            "color": "b",
        },
        {
            "path": net_dir / "CLR_max",
            "label": "decor-CLR",
            "score_name": "decor_scores",
            "color": "r",
            "linestyle": "--",
        },
        {
            "path": net_dir / "transformer_restricted",
            "label": "decor-Trans",
            "score_name": "decor_score",
            "color": "g",
            "linestyle": "--",
        },
        {
            "path": net_dir / "graph_restricted",
            "label": "decor-Graph",
            "score_name": "decor_score",
            "color": "b",
            "linestyle": "--",
        },
    ]

    processes = {
        "QCD": "QCD_jj_pt_550_650_test.h5",
        # "WZ": "WZ_jjnunu_pT_550_650_test.h5",
        "ttbar": "ttbar_allhad_pt_550_650_test.h5",
    }

    ## Plot the inclusive roc curves for each combination of processes dict
    plot_roc_curves(
        output_dir, networks, processes, br_at_eff=[0.3, 0.7], ylim=(1, 1e5)
    )
    exit()

    ## Plot the mass sculpting (Jensen-Shannon) as a function of the background rej
    plot_mass_sculpting(
        output_dir,
        networks,
        "QCD",
        "QCD_jj_pt_550_650_test.h5",
        data_dir,
        bins=np.linspace(0, 300, 40),
        br_values=[0.5, 0.8, 0.9, 0.95, 0.99],
        xlim=[0.4, 1.1],
    )

    ## For each network make seperate plots
    for net in networks:

        # Plot the distributions of the scores for each class overlaid
        # plot_score_distributions(
        #     output_dir,
        #     net,
        #     processes,
        #     np.linspace(0, 1, 50),
        #     xlim=[0, 1],
        #     score_scalling_fn=sigmoid,
        #     do_log=True,
        # )

        # Plot heatmaps showing the correlation between the mass and the scores
        # plot_mass_score_correlation(
        #     output_dir,
        #     net,
        #     "QCD",
        #     "QCD_jj_pt_550_650_test.h5",
        #     data_dir,
        #     mass_bins = np.linspace(0, 300, 40),
        #     score_bins = np.linspace(0, 1, 40),
        #     # score_scalling_fn = sigmoid,
        #     # do_log=True,
        # )

        # Plot the mass distributions with ratios for different background rejections
        plot_mass_distributions(
            output_dir,
            net,
            processes,
            data_dir,
            bins=np.linspace(0, 450, 40),
            br_values=[0.5, 0.8, 0.9, 0.95, 0.99],
            # ylim = [0, 0.02],
            ratio_ylim=[0, 1],
            do_log=False,
            do_norm=False,
            redo_quantiles=False,
            fig_size=(4 * len(processes), 4),
        )


if __name__ == "__main__":
    main()
