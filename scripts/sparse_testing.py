import torch as T
import torch.nn as nn

from mattstools.torch_utils import sparse_from_mask


def main():

    ## Parameters
    n_batch = 3
    n_nodes = 4
    n_feats = 2
    e_feats = 2

    ## Dummy tensors
    adjmat = T.rand((n_batch, n_nodes, n_nodes)) > 0.8
    e_feats = T.rand((n_batch, n_nodes, n_nodes, e_feats), dtype=T.float)
    e_feats[~adjmat] = 0

    ## Network
    net = nn.Linear(2, 5)

    print(adjmat)

    sparse_e = sparse_from_mask(e_feats, adjmat)
    sparse_e.requires_grad = True
    values = sparse_e.values()

    out = net(values)

    # print(out)


if __name__ == "__main__":
    main()
