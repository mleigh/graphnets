"""
Playground to testing and debugging
"""

import time
import torch as T
from Resources.utils import scatter_bc


def main():

    ## Parameters
    n_batch = 3
    n_nodes = 4
    n_feats = 2

    ## Generate the graph data
    mask = T.rand((n_batch, n_nodes)) > 0.7
    mask[:, 0] = True
    nodes = T.rand((n_batch, n_nodes, n_feats))
    nodes[~mask] = 0
    adjmat = mask.unsqueeze(-1) * mask.unsqueeze(-2)

    ## Create the compressed difference matrix
    ex_size = (*adjmat.shape, n_feats)
    send_info = nodes.unsqueeze(-2).expand(ex_size)[adjmat]
    recv_info = nodes.unsqueeze(-3).expand(ex_size)[adjmat]
    diff_info = send_info - recv_info
    # print(diff_info)

    ## Try the above using compressed nodes
    cmp_nodes = nodes[mask]

    ## Get the send receive indexes of the flattened adjmat
    b_totals = mask.sum(-1).cumsum_(0)
    b_totals = T.cat([T.tensor([0]), b_totals[:-1]])

    indexes = T.nonzero(adjmat).T
    indexes = indexes + b_totals[indexes[0]]
    indexes = indexes[1:]
    print(b_totals)
    print(adjmat)
    print(indexes)
    exit()
    send_info = cmp_nodes[indexes[0]]
    recv_info = cmp_nodes[indexes[1]]
    diff_info = send_info - recv_info
    print(diff_info)
    exit()
    ## Aggregation by using a sparse matrix

    print(pooled)


if __name__ == "__main__":
    main()
