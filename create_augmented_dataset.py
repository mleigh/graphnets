"""
Main callable script to train the graph classifier
"""

import h5py
from tqdm import tqdm
from pathlib import Path

from torch.utils.data import DataLoader
from gnets.datasets import AugJetData
from gnets.utils import get_disc_configs

from mattstools.plotting import plot_multi_hists


def save_to_file(out_dir, out_name, loader):
    """Save the jet data to a new file"""

    ## Initialise the dataset
    file = h5py.File(out_dir / (out_name + ".h5"), "w")
    orig_set = file.create_dataset(
        "originals", (0, 100, 3), chunks=True, maxshape=(None, 100, 3)
    )
    augm_set = file.create_dataset(
        "augmented", (0, 10, 4), chunks=True, maxshape=(None, 10, 4)
    )

    ## Cycle through the data and add to the HDF file
    for batch in tqdm(loader, "training data"):
        orig_nodes, augm_nodes = batch

        ## Make the datasets bigger
        orig_set.resize(orig_set.shape[0] + orig_nodes.shape[0], axis=0)
        augm_set.resize(augm_set.shape[0] + augm_nodes.shape[0], axis=0)

        ## Insert the variables in the latest extended space
        orig_set[-orig_nodes.shape[0] :] = orig_nodes.numpy()
        augm_set[-augm_nodes.shape[0] :] = augm_nodes.numpy()
    file.close()


def main():
    """Run the script"""

    #######################
    out_dir = Path("/home/users/l/leighm/scratch/Data/AugmentedJets")
    #######################

    ## Collect the session arguments, returning the configurations for the session
    data_conf, _, train_conf = get_disc_configs()

    ## Load and save the training set
    train_set = AugJetData(dset="train", **data_conf)
    train_loader = DataLoader(train_set, **train_conf.loader_kwargs)
    save_to_file(out_dir, "train", train_loader)

    ## Load and save the testing set
    test_set = AugJetData(dset="test", **data_conf)
    test_loader = DataLoader(test_set, **train_conf.loader_kwargs)
    save_to_file(out_dir, "test", test_loader)

    ## Plot some histograms to check that it worked
    with h5py.File(out_dir / "test.h5", "r") as test_file:
        orig = test_file["originals"][:5_000]
        augm = test_file["augmented"][:5_000]
        orig = orig[orig.sum(axis=-1) != 0]
        augm = augm[augm.sum(axis=-1) != 0]

        plot_multi_hists(
            [orig, augm],
            ["originals", "augmented"],
            train_set.coordinates.node,
            path = "here",
        )


if __name__ == "__main__":
    main()
