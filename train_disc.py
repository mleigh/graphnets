"""
Main callable script to train the graph classifier
"""

from joblib import load

from torch.utils.data import DataLoader

from mattstools.trainer import Trainer
from mattstools.torch_utils import train_valid_split, get_max_cpu_suggest
from mattstools.gnets.graphs import gcoll

from gnets.datasets import JetData
from gnets.networks import (
    DeepSetClassifier,
    DenseClassifier,
    GraphNetClassifier,
    TransformerClassifier,
)
from gnets.utils import get_disc_configs

import wandb


def main():
    """Run the script"""

    ## Collect the session arguments, returning the configurations for the session
    data_conf, net_conf, train_conf = get_disc_configs()

    ## Initialise the weights and biases session
    wandb.init(
        entity="mleigh",
        project="JetTaggerReduced",
        name=net_conf.base_kwargs.name,
        resume=train_conf.trainer_kwargs.resume,
        id=train_conf.wandb_id or wandb.util.generate_id(),
    )
    train_conf.wandb_id = wandb.run.id

    ## Initialise the datasets for RODEM data
    train_set = JetData(dset="train", **data_conf)

    ## Get the data dimensions and add them the network configuraiton
    graph_dim, ctxt_dim, _ = train_set.get_dim()
    n_classes = train_set.n_classes
    net_conf.base_kwargs.inpt_dim = graph_dim
    net_conf.base_kwargs.outp_dim = n_classes if n_classes > 2 else 1  # Logit-Reg
    net_conf.base_kwargs.ctxt_dim = ctxt_dim
    net_conf.n_nodes = train_set.get_n_nodes()

    ## Create the network (assume type based on present configs)
    if net_conf.base_kwargs.net_type == "graph":
        network = GraphNetClassifier(**net_conf)
    elif net_conf.base_kwargs.net_type == "deepset":
        network = DeepSetClassifier(**net_conf)
    elif net_conf.base_kwargs.net_type == "transformer":
        network = TransformerClassifier(**net_conf)
    elif net_conf.base_kwargs.net_type == "dense":
        network = DenseClassifier(**net_conf)
    print(network)

    ## Save the proprocessing information or reload if resuming a job
    if train_conf.trainer_kwargs.resume:
        train_set.scalers = load(network.full_name / "scalers")
    else:
        train_set.plot_fit_and_save_preprocess(network.full_name)

    ## Create the validation and training sets
    if "rodem" in data_conf.path:  ## RODEM data manually split
        train_set, valid_set = train_valid_split(train_set, train_conf.val_frac)
    else:  ## TopTag data just load the validation set and copy scalers
        valid_set = JetData(dset="val", **data_conf)
        valid_set.scalers = train_set.scalers

    ## Make the training and validation loaders
    train_loader = DataLoader(
        train_set, **train_conf.loader_kwargs, collate_fn=gcoll, shuffle=True
    )
    valid_loader = DataLoader(valid_set, **train_conf.loader_kwargs, collate_fn=gcoll)

    ## Create the save folder for the network and store the configuration dicts
    network.save_configs(data_conf, net_conf, train_conf)

    ## Create the trainer and run the loop
    trainer = Trainer(network, train_loader, valid_loader, **train_conf.trainer_kwargs)
    trainer.run_training_loop()


if __name__ == "__main__":
    main()
