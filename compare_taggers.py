"""
Example plotting script
"""

from pathlib import Path
import numpy as np

from jutils.plotting import (
    plot_roc_curves,
    plot_score_distributions,
    plot_sculpted_distributions,
    plot_mass_sculpting,
    plot_mass_score_correlation,
)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))


# dir = Path("/home/users/l/leighm/scratch/WhitePaperExports/WithEdges")
# net_list = list()
# for net in dir.glob("*"):
#     for file in net.glob("*train.h5"):
#         file.rename(str(file).replace("train", "test"))
#         print(file)


def main():
    """Main script"""

    output_dir = "plots/white_paper/decor_vae/"
    data_dir = Path("/srv/beegfs/scratch/groups/rodem/anomalous_jets/virtual_data/")
    white_dir = Path("/home/users/l/leighm/scratch/WhitePaperExports/")
    cst_dir = white_dir / "QCDvsTop"
    hig_dir = white_dir / "OnlyHigh"
    edg_dir = white_dir / "WithEdges"
    aug_dir = white_dir / "Augmentations"
    uns_dir = white_dir / "Unsupervised"
    exp_dir = Path("/srv/beegfs/scratch/groups/rodem/exported_taggers/")
    networks = [
        ###
        # {"path": hig_dir / "denseHigh_ttbar", "label": "Dense (Synthetic)"},
        # {"path": cst_dir / "dense20",         "label": "Dense 20"},
        # {"path": cst_dir / "dense32",         "label": "Dense 32"},
        # {"path": cst_dir / "dense64",         "label": "Dense"},
        # {"path": cst_dir / "dense100", "label": "Dense"},
        # {"path": cst_dir / "deepset20",       "label": "Deepset 20"},
        # {"path": cst_dir / "deepset32",       "label": "Deepset 32"},
        # {"path": cst_dir / "deepset64",       "label": "Deepset 64"},
        # {"path": cst_dir / "deepset100", "label": "Deepset"},
        # {"path": cst_dir / "graph20",         "label": "Graph 20"},
        # {"path": cst_dir / "graph32",         "label": "Graph 32"},
        # {"path": cst_dir / "graph64",         "label": "Graph 64"},
        # {"path": cst_dir / "graph100", "label": "Graph"},
        # {"path": cst_dir / "transformer20",   "label": "Transformer 20"},
        # {"path": cst_dir / "transformer32",   "label": "Transformer 32"},
        # {"path": cst_dir / "transformer64",   "label": "Transformer"},
        # {"path": cst_dir / "transformer100", "label": "Transformer"},
        ###
        # {"path": edg_dir / "graph_ttbar_1",  "label": "EdgeGraph 0.1"},
        # {"path": edg_dir / "graph_ttbar_3",  "label": "EdgeGraph 0.3"},
        # {"path": edg_dir / "graph_ttbar_5",  "label": "EdgeGraph 0.5"},
        # {"path": edg_dir / "graph_ttbar_999",  "label": "EdgeGraph FC"},
        {
            "path": edg_dir / "transformer_ttbar_999",
            "label": "ParT",
            "linestyle": "solid",
            "color": "black",
        },
        ###
        # {"path": aug_dir / "transformer_ttbar_boost-005",  "label": "Boost"},
        # {"path": aug_dir / "transformer_ttbar_merge-005",  "label": "Merge"},
        # {"path": aug_dir / "transformer_ttbar_rotate",  "label": "Rotate"},
        # {"path": aug_dir / "transformer_ttbar_smear",  "label": "Smear"},
        # {"path": aug_dir / "transformer_ttbar_split-10",  "label": "Split"},
        # {"path": aug_dir / "transformer_ttbar_crop-10",  "label": "Crop"},
        ###
        # {"path": exp_dir / "VAEvirt_20lt_1b_editable",  "label": "VAE", "score_name": "mse", "linestyle": "-"},
        # {"path": exp_dir / "VAEvirt_20lt_1b_editable",  "label": "decor-VAE", "score_name": "decor_mse", "linestyle": "--"},
        # {"path": uns_dir / "TransVAE_EMMD_16",  "label": "EMMD-SKHN", "score_name": "sinkhorn"},
        # {"path": uns_dir / "TransVAE_EMMD_16",  "label": "EMMD-EMMD", "score_name": "energy_mmd"},
        # {"path": uns_dir / "TransVAE_EMMD_16",  "label": "EMMD-KLD", "score_name": "kld"},
        # {"path": uns_dir / "TransVAE_SKHN_16",  "label": "SKHN-SKHN", "score_name": "sinkhorn"},
        # {"path": uns_dir / "TransVAE_SKHN_16",  "label": "SKHN-EMMD", "score_name": "energy_mmd"},
        # {"path": uns_dir / "TransVAE_SKHN_16",  "label": "SKHN-KLD", "score_name": "kld"},
    ]

    for net in networks:

        # if "Unsupervised" in str(net["path"]):
        #     if "EMMD" in str(net["path"]):
        #         net["color"] = "red"
        #     if "SKHN" in str(net["path"]):
        #         net["color"] = "blue"
        #     if net["score_name"] == "sinkhorn":
        #         net["linestyle"] = "solid"
        #     if net["score_name"] == "energy_mmd":
        #         net["linestyle"] = "dashed"
        #     if net["score_name"] == "kld":
        #         net["linestyle"] = "dotted"

        # if "vsTop" in str(net["path"]):
        #     if "dense" in str(net["path"]):
        #         net["color"] = "orange"
        #     if "deepset" in str(net["path"]):
        #         net["color"] = "red"
        #     if "graph" in str(net["path"]):
        #         net["color"] = "green"
        #     if "transformer" in str(net["path"]):
        #         net["color"] = "blue"

        # if "20" in str(net["path"]):
        #     net["linestyle"] = "dotted"
        # if "32" in str(net["path"]):
        #     net["linestyle"] = "dashdot"
        # if "64" in str(net["path"]):
        #     net["linestyle"] = "dashed"
        # if "100" in str(net["path"]):
        #     net["linestyle"] = "solid"

        # if "High" in str(net["path"]):
        #     net["color"] = "black"
        #     net["linestyle"] = "solid"

        # if "1" in str(net["path"]):
        #     net["linestyle"] = "dotted"
        # if "3" in str(net["path"]):
        #     net["linestyle"] = "dashdot"
        # if "5" in str(net["path"]):
        #     net["linestyle"] = "dashed"
        # if "999" in str(net["path"]):
        #     net["linestyle"] = "solid"

        if "Unsupervised" not in str(net["path"]):
            net["score_name"] = "score"

    processes = {
        "QCD": "QCD_jj_pt_450_1200_test.h5",
        "WZ": "WZ_allhad_pt_450_1200_test.h5",
        "ttbar": "ttbar_allhad_pt_450_1200_test.h5",
        # "H2": "H2tbtb_1800_HC_400_test.h5",
    }

    # for net in networks.copy():
    #     if not (Path(net["path"])/processes["ttbar"]).is_file():
    #         networks.remove(net)

    ## Plot the inclusive roc curves for each combination of processes dict
    # plot_roc_curves(output_dir, networks, processes, br_at_eff=[0.3, 0.7], ylim=(1, 1e5))
    # exit()

    ## Plot the mass sculpting (Jensen-Shannon) as a function of the background rej
    # plot_mass_sculpting(
    #     output_dir,
    #     networks,
    #     "QCD",
    #     "QCD_jj_pt_450_1200_test.h5",
    #     data_dir,
    #     bins=np.linspace(0, 300, 40),
    #     br_values=[0.5, 0.8, 0.9, 0.95, 0.99],
    #     xlim=[0.4, 1.1],
    # )

    ## For each network make seperate plots
    for net in networks:

        # Plot the distributions of the scores for each class overlaid
        # plot_score_distributions(
        #     output_dir,
        #     net,
        #     processes,
        #     np.linspace(0, 1, 50),
        #     xlim=[0, 1],
        #     score_scalling_fn=sigmoid,
        #     do_log=True,
        # )

        # Plot heatmaps showing the correlation between the mass and the scores
        # plot_mass_score_correlation(
        #     output_dir,
        #     net,
        #     "QCD",
        #     "QCD_jj_pt_450_1200_test.h5",
        #     data_dir,
        #     mass_bins = np.linspace(0, 300, 40),
        #     score_bins = np.linspace(0, 1, 40),
        #     score_scalling_fn = sigmoid,
        #     do_log=True,
        # )

        # Plot the mass, pt and eta distributions
        for var, bins in [
            ("mass", np.linspace(0, 450, 40)),
            ("pt", np.linspace(450, 1250, 40)),
            ("eta", np.linspace(-3.5, 3.5, 40)),
        ]:
            plot_sculpted_distributions(
                output_dir,
                net,
                processes,
                data_dir,
                var=var,
                bins=bins,
                br_values=[0.5, 0.8, 0.9, 0.95, 0.99],
                ratio_ylim=[0, 1],
                do_log=False,
                do_norm=False,
                redo_quantiles=False,
                fig_size=(4 * len(processes), 4),
            )


if __name__ == "__main__":
    main()
