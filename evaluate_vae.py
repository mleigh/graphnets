from joblib import load
from pathlib import Path

import numpy as np
from tqdm import tqdm

import torch as T
from torch.utils.data import DataLoader

from mattstools.utils import load_yaml_files
from mattstools.torch_utils import sel_device, move_dev, to_np
from mattstools.plotting import plot_multi_hists, plot_corr_heatmaps
from mattstools.gnets.graphs import gcoll

from gnets.datasets import JetData
from gnets.jets import locals_to_jet

T.set_grad_enabled(False)
T.manual_seed(42)


def main():

    ###############################################
    b_size = 256
    n_wrkrs = 4
    n_jets = 100_000
    device = sel_device("gpu")

    net_folder = "/home/users/l/leighm/scratch/Saved_Networks/JetVAEs"
    net_names = [
        "GraphVAE_SFTMX",
    ]
    outdir = Path("plots/vae_sftmax")

    procs = ["QCD", "WZ", "ttbar"]
    ###############################################

    ## Cycle through the processes (look at each one individually)
    for proc in procs:
        print(proc)
        old_data_conf = {}

        ## Cycle through the networks
        for net_nm in net_names:
            print(net_nm)

            ## Load the network
            model_folder = Path(net_folder, net_nm)
            network = T.load(Path(model_folder, "best"), map_location=device)
            network.set_device(device)
            network.eval()

            ## Load the data config and check if it is new
            data_conf = load_yaml_files(Path(model_folder, "config/data.yaml"))
            data_conf["datasets"] = {"c1": [proc]}
            data_conf["n_jets"] = n_jets

            ## Load new data if needed, otherwise data is reused
            if data_conf != old_data_conf:
                dataset = JetData(dset="test", **data_conf)
                dataset.scalers = load(network.full_name / "scalers")
                old_data_conf = data_conf

            ## Create a simple dataloader to allow us to batch the data quickly
            loader = DataLoader(
                dataset,
                batch_size=b_size,
                collate_fn=gcoll,
                drop_last=False,
                shuffle=False,
                num_workers=n_wrkrs,
            )

            ## Create running stores of the rec and generated nodes
            org_nodes = []
            rec_nodes = []
            gen_nodes = []
            mask = []

            ## Iterate through the dataloader
            for sample in tqdm(loader, desc="plot", ncols=80):
                graphs, _ = move_dev(sample, device)
                rec_graphs, _, _, _ = network(graphs)

                ## Reconstruct and generate the nodes
                org_nodes.append(to_np(graphs.nodes))
                rec_nodes.append(to_np(rec_graphs.nodes))
                gen_nodes.append(
                    to_np(network.generate_from_noise(graphs.mask, graphs.cndts).nodes)
                )
                mask.append(to_np(graphs.mask))

            ## Combine into a single numpy array
            org_nodes = np.concatenate(org_nodes)
            rec_nodes = np.concatenate(rec_nodes)
            gen_nodes = np.concatenate(gen_nodes)
            mask = np.concatenate(mask)

            ## Create histogram plots of the node distributions
            plot_multi_hists(
                outdir / f"{net_nm}_{proc}_consts",
                [org_nodes[mask], rec_nodes[mask], gen_nodes[mask]],
                ["Original", "Reconstructed", "Generated"],
                [
                    r"$\Delta\eta$",
                    r"$\Delta\phi$",
                    r"$\mathrm{log}(\frac{p_{T}}{p^{jet}_{T}})$",
                ],
            )

            ## Create histogram plots of the jet distributions
            plot_multi_hists(
                outdir / f"{net_nm}_{proc}_jets",
                [
                    locals_to_jet(org_nodes, mask, dataset.high_data),
                    locals_to_jet(rec_nodes, mask, dataset.high_data),
                    locals_to_jet(gen_nodes, mask, dataset.high_data),
                ],
                ["Original", "Reconstructed", "Generated"],
                [r"Total Jet $p_{T}$ [GeV]", "Jet mass [GeV]"],
                bins=[np.linspace(0, 1500, 100), np.linspace(0, 350, 100)],
            )

            ## Create a 2D plot of the distributions
            for nodes, name in zip(
                [org_nodes, rec_nodes, gen_nodes],
                ["Original", "Reconstructed", "Generated"],
            ):
                plot_corr_heatmaps(
                    outdir / f"{net_nm}_{proc}_{name}_heatmap",
                    nodes[mask][..., 0],
                    nodes[mask][..., 1],
                    bins=[np.linspace(-1, 1, 40), np.linspace(-1, 1, 40)],
                    xlabel=r"$\Delta\eta$",
                    ylabel=r"$\Delta\phi$",
                    do_log=True,
                    weights=nodes[mask][..., 2],
                    incl_line=False,
                    incl_cbar=True,
                    title="name",
                    figsize=(6, 5),
                )


if __name__ == "__main__":
    main()
