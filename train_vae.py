"""
Main callable script to train the graph autoencoder
"""

from joblib import load

from torch.utils.data import DataLoader

from mattstools.trainer import Trainer
from mattstools.torch_utils import train_valid_split, get_max_cpu_suggest
from mattstools.gnets.graphs import gcoll

from gnets.datasets import JetData
from gnets.networks import TransformerVAE
from gnets.utils import get_vae_configs

import wandb


def main():
    """Run the script"""

    ## Collect the session arguments, returning the configurations for the session
    data_conf, net_conf, train_conf = get_vae_configs()

    ## Initialise the weights and biases session
    wandb.init(
        entity="mleigh",
        project="WhitePaper3VAEs",
        name=net_conf.base_kwargs.name,
        resume=train_conf.trainer_kwargs.resume,
        id=train_conf.wandb_id or wandb.util.generate_id(),
    )
    train_conf.wandb_id = wandb.run.id

    ## Initialis    e the datasets
    train_set = JetData(dset="train", **data_conf)

    ## Get the data dimensions and add them the network configuraiton (inpt=outp)
    graph_dim, ctxt_dim, _ = train_set.get_dim()
    net_conf.base_kwargs.inpt_dim = graph_dim
    net_conf.base_kwargs.outp_dim = graph_dim
    net_conf.base_kwargs.ctxt_dim = ctxt_dim
    net_conf.n_nodes = train_set.get_n_nodes()

    ## Create the network
    network = TransformerVAE(**net_conf)

    ## Save the proprocessing information or reload if resuming a job
    if train_conf.trainer_kwargs.resume:
        train_set.scalers = load(network.full_name / "scalers")
    else:
        train_set.plot_fit_and_save_preprocess(network.full_name)

    ## Create the validation and training dataloaders
    train_set, valid_set = train_valid_split(train_set, train_conf.val_frac)
    train_loader = DataLoader(train_set, **train_conf.loader_kwargs, collate_fn=gcoll)
    valid_loader = DataLoader(valid_set, **train_conf.loader_kwargs, collate_fn=gcoll)

    ## Create the save folder for the network and store the configuration dicts
    network.save_configs(data_conf, net_conf, train_conf)

    ## Load the trainer and run the loop
    trainer = Trainer(network, train_loader, valid_loader, **train_conf.trainer_kwargs)
    trainer.run_training_loop()


if __name__ == "__main__":
    main()
