"""
Collection of pytorch lightning modules to use as neural networks
"""

# pylint: disable=arguments-differ, attribute-defined-outside-init

from typing import Tuple, Union

import matplotlib.pyplot as plt
import numpy as np

import torch as T
from torch.utils.data import DataLoader
from torch.nn.functional import softplus


from mattstools.modules import DenseNetwork, DeepSet
from mattstools.plotting import plot_latent_space, plot_multi_hists
from mattstools.network import MyNetBase
from mattstools.loss import kld_to_norm, kld_with_OE, masked_dist_loss
from mattstools.torch_utils import (
    get_loss_fn,
    smart_cat,
    to_np,
    decompress,
    reparam_trick,
    log_squash,
)
from mattstools.utils import undo_log_squash, log_squash

from mattstools.gnets.graphs import GraphBatch, gcoll
from mattstools.gnets.modules import FullGraphVectorEncoder
from mattstools.gnets.plotting import plot_scatter
from mattstools.transformers import (
    FullTransformerVectorEncoder,
    FullTransformerVectorDecoder,
)

from gnets.jets import cstetaphipt_to_jetmass, cstpxpypz_to_jetmass

import wandb


class DenseClassifier(MyNetBase):
    """A dense classifier which can take in a graph object
    - The graph nodes are flattened and padded with 0's
    - Then concatentated with globals and conditionals
    """

    def __init__(
        self,
        base_kwargs: dict,
        ctxt_dim: int = 0,
        n_nodes: int = None,
        loss_nm: str = "none",
        dns_kwargs: dict = None,
    ) -> None:
        """
        kwargs:
            ctxt_dim: The dimension of the contextual tensor
            n_nodes: The max number of nodes used to train this graph
            loss_nm: The name of the loss function to apply to the outputs
            dns_kwargs: Keyword arguments for the dense network
        """
        super().__init__(**base_kwargs)

        ## Dict default kwargs
        dns_kwargs = dns_kwargs or {}

        ## Save the loss function, update the input dim and create the network
        self.n_nodes = n_nodes
        self.ctxt_dim = ctxt_dim
        self.loss_fn = get_loss_fn(loss_nm)

        ## The modules that makes up the classifier
        self.dense = DenseNetwork(
            inpt_dim=self.inpt_dim[1] * n_nodes + self.inpt_dim[2] + ctxt_dim,
            outp_dim=self.outp_dim,
            ctxt_dim=ctxt_dim,
            **dns_kwargs,
        )
        self.to(self.device)

    def forward(self, inputs: GraphBatch, ctxt=None) -> T.Tensor:
        """Flatten and pass the input graph through the dense network"""

        ## Flatten the nodes and use mask broadcasting to ensure 0 padding
        flat_nodes = T.flatten(inputs.nodes * inputs.mask.unsqueeze(-1), start_dim=1)

        ## Combine the inputs into a single layer
        all_inpt = smart_cat([flat_nodes, inputs.globs], dim=-1)

        ## Return outputs of dense network
        return self.dense(all_inpt, ctxt=ctxt)

    def get_losses(self, sample, _batch_idx: int, _epoch_num: int) -> dict:
        graph, ctxt, label = sample
        return {"total": self.loss_fn(self.forward(graph, ctxt), label).mean()}


class DeepSetClassifier(MyNetBase):
    """A graph to vector mapping for graph data which uses a deep set over the nodes
    - Will also include the global and conditional information as context
    """

    def __init__(
        self,
        base_kwargs: dict,
        ctxt_dim: int = 0,
        n_nodes: int = None,
        loss_nm: str = "none",
        dpst_kwargs: dict = None,
    ) -> None:
        """
        kwargs:
            ctxt_dim: The dimension of the contextual tensor
            n_nodes: The max number of nodes used to train this graph
            loss_nm: The name of the loss function to apply to the outputs
            dpst_kwargs: The keyword arguments to initialise DeepSet
        """
        super().__init__(**base_kwargs)

        ## Dict default kwargs
        dpst_kwargs = dpst_kwargs or {}

        ## Save the class attributes
        self.n_nodes = n_nodes
        self.ctxt_dim = ctxt_dim
        self.loss_fn = get_loss_fn(loss_nm)

        ## The modules that makes up the classifier
        self.deep_set = DeepSet(
            self.inpt_dim[1],  ## Only takes the nodes
            self.outp_dim,
            ctxt_dim=self.inpt_dim[2] + ctxt_dim,
            **dpst_kwargs,
        )
        self.to(self.device)

    def forward(self, inputs: GraphBatch, ctxt=None) -> T.Tensor:
        """Pass the nodes through the deep set, with high level conditioning info"""
        return self.deep_set(inputs.nodes, inputs.mask, [inputs.globs, ctxt])

    def get_losses(self, sample, _batch_idx: int, _epoch_num: int) -> dict:
        graph, ctxt, label = sample
        return {"total": self.loss_fn(self.forward(graph, ctxt), label).mean()}


class GraphNetClassifier(MyNetBase):
    """A graph to vector mapping for graph data using graph networks

    Contains the following modules
     - GraphNetwork: A G2G mapping using multiple GNblocks with EdgeBuilders
     - DenseNetwork: Final mapping to move the output space
    """

    def __init__(
        self,
        *,
        base_kwargs: dict,
        n_nodes: int = None,
        loss_nm: str = "none",
        fgve_kwargs: dict = None,
    ) -> None:
        """
        kwargs:
            n_nodes: The max number of nodes used to train this graph
            loss_nm: The name of the loss function
            genc_kwargs: The keyword arguments to initialise the GraphNetEncoder
        """
        super().__init__(**base_kwargs)

        ## Dict default kwargs, copy makes it safe when we change output
        fgve_kwargs = fgve_kwargs.copy() or {}

        ## Save the class attributes
        self.n_nodes = n_nodes
        self.loss_fn = get_loss_fn(loss_nm)

        ## The series of modules that make up the network
        self.graph_enc = FullGraphVectorEncoder(
            inpt_dim=self.inpt_dim,
            outp_dim=self.outp_dim,
            ctxt_dim=self.ctxt_dim,
            **fgve_kwargs,
        )
        self.to(self.device)

    def forward(self, graphs: GraphBatch, ctxt: T.Tensor = None) -> T.Tensor:
        """Get the vector encoding by passing through the graph network"""
        return self.graph_enc(graphs, ctxt)

    def get_losses(self, sample, _batch_idx: int, _epoch_num: int) -> dict:
        graph, ctxt, label = sample
        return {"total": self.loss_fn(self.forward(graph, ctxt), label).mean()}


class TransformerClassifier(MyNetBase):
    """A transformer classifier which can take in a graph object
    - Edges are used to augment the attention weights
    """

    def __init__(
        self,
        base_kwargs: dict,
        n_nodes: int = None,
        loss_nm: str = "none",
        ftve_kwargs: dict = None,
    ) -> None:
        """
        kwargs:
            n_nodes: The max number of nodes used to train this graph
            loss_nm: The name of the loss function
            node_emb_kwargs: Keyword arguments for the node embedding network
            ftve_kwargs: The keyword arguments to initialise the FullTransformerEncoder
        """
        super().__init__(**base_kwargs)

        ## Safe dict default kwargs
        ftve_kwargs = ftve_kwargs or {}

        ## Save the class attributes
        self.n_nodes = n_nodes
        self.loss_fn = get_loss_fn(loss_nm)
        self.has_edges = self.inpt_dim[0] > 0

        ## The FTVE module
        self.trans_enc = FullTransformerVectorEncoder(
            inpt_dim=self.inpt_dim[1],
            outp_dim=self.outp_dim,
            ctxt_dim=self.ctxt_dim,
            edge_dim=self.inpt_dim[0],
            **ftve_kwargs,
        )
        self.to(self.device)

    def forward(self, graphs: GraphBatch, ctxt: T.Tensor = None) -> T.Tensor:
        """Get the vector encoding by passing through the graph network"""
        return self.trans_enc(
            nodes=graphs.nodes,
            mask=graphs.mask,
            ctxt=ctxt,
            edges=decompress(graphs.edges, graphs.adjmat) if self.has_edges else None,
        )

    def get_losses(self, sample, _batch_idx: int, _epoch_num: int) -> dict:
        graph, ctxt, label = sample
        return {"total": self.loss_fn(self.forward(graph, ctxt), label).mean()}


class TransformerVAE(MyNetBase):
    """An flow autoencoder for point cloud data using transformers"""

    def __init__(
        self,
        *,
        base_kwargs: dict,
        ctxt_dim: int = 0,
        n_nodes: int = None,
        lat_dim: int = 2,
        do_vae: bool = False,
        do_outlier_exposure: bool = False,
        gen_loss_name: str = "sinkhorn",
        kld_weight_cycle: Union[tuple, list] = (0, 1, 100),
        exp_final: False,
        ftve_kwargs: dict = None,
        ftvd_kwargs: dict = None,
    ) -> None:
        """
        kwargs:
            ctxt_dim: Dim. of the context tensor used in both encoder and decoder
            n_nodes: The max number of nodes used to train this graph
            lat_dim: The dimension of the latent space for encoding or sampling
            do_vae: Apply the reparameterisation trick and include a kld loss term
            do_outlier_exposure: Apply the OE loss term for the latent space
            gen_loss_name: Name of the point cloud generation loss term
            kld_weight_cycle: The weight for the KLD loss (min, max, niter)
            exp_final: Apply exponent to the last column of the node output
            ftve_kwargs: The keyword arguments for the FTVE (encoder)
            ftvd_kwargs: The keyword arguments for the FTVG (generator)
        """
        super().__init__(**base_kwargs)

        ## Safe dict defaults
        ftve_kwargs = ftve_kwargs or {}
        ftvd_kwargs = ftvd_kwargs or {}

        ## Save some attributes
        self.n_nodes = n_nodes
        self.ctxt_dim = ctxt_dim
        self.lat_dim = lat_dim
        self.do_vae = do_vae
        self.do_outlier_exposure = do_outlier_exposure
        self.gen_loss_name = gen_loss_name
        self.gen_loss_fn = get_loss_fn(gen_loss_name)
        self.kld_weight_cycle = kld_weight_cycle
        self.exp_final = exp_final

        ## Buffer allows it to be saved and reloaded!
        self.register_buffer("kld_weight", T.tensor(kld_weight_cycle[0]).float())

        ## To tell if the transformer needs space for edge or context features
        self.has_ctxt = ctxt_dim > 0 or self.inpt_dim[2] > 0
        self.has_edges = self.inpt_dim[0] > 0

        ## Initialise the transformer encoding model
        self.encoder = FullTransformerVectorEncoder(
            inpt_dim=self.inpt_dim[1],
            outp_dim=self.lat_dim * (1 + self.do_vae),
            ctxt_dim=self.ctxt_dim + self.inpt_dim[2],
            edge_dim=self.inpt_dim[0],
            **ftve_kwargs,
        )

        ## Initialise the transformer generator model
        self.generator = FullTransformerVectorDecoder(
            inpt_dim=self.lat_dim,
            outp_dim=self.inpt_dim[1],
            ctxt_dim=self.ctxt_dim + self.inpt_dim[2],
            **ftvd_kwargs,
        )

        ## The names of the losses used in this network (already has total be default)
        self.loss_names = ["total", "lat_loss", "gen_loss"]

        ## Move everything to the device and print
        self._setup()

    def _step_kld_weight(self):
        """Increment the kld weight linearly to a max value"""
        self.kld_weight = T.min(
            T.tensor(self.kld_weight_cycle[1]),
            self.kld_weight
            + (self.kld_weight_cycle[1] - self.kld_weight_cycle[0])
            / self.kld_weight_cycle[2],
        ).float()

    def _encode(self, graphs: GraphBatch, ctxt: T.Tensor = None) -> tuple:
        """Full encoding step, which includes any extra latent space operation"""

        ## Get the raw latent space encodings
        latents = self.encoder(
            graphs.nodes,
            graphs.mask,
            decompress(graphs.edges, graphs.adjmat) if self.has_edges else None,
            smart_cat([graphs.globs, ctxt]) if self.has_ctxt else None,
        )

        ## Apply the latent space operation and return
        if self.do_vae:
            return reparam_trick(latents)
        return latents, None, None

    def _generate(
        self, latents: T.Tensor, mask: T.BoolTensor, ctxt: T.Tensor
    ) -> T.Tensor:
        """Full generation step, which includes any extra output operation"""

        ## Pass through the generator, giving the original mask and context
        rec_nodes = self.generator(latents, mask, ctxt=ctxt)

        ## Apply a softmax to last column of the nodes
        if self.exp_final:
            last_dim = rec_nodes[..., -1].clone()  # Needed or pytorch will complain
            last_dim[~mask] = -T.inf
            last_dim = last_dim.exp()
            rec_nodes[..., -1] = last_dim

        return rec_nodes

    def forward(self, graphs: GraphBatch, ctxt: T.Tensor = None) -> tuple:
        """Pass inputs through the full autoencoder, returning the encoding params and
        the reconstructed nodes.
        """
        latents, means, lstds = self._encode(graphs, ctxt)
        rec_nodes = self._generate(latents, graphs.mask, ctxt)
        return rec_nodes, latents, means, lstds

    def generate_from_noise(self, mask: T.BoolTensor, ctxt: T.Tensor = None):
        """Generate random graphs using noise as the latent vector.
        Still requires the conditional labels and the requested mask!
        """
        latents = T.randn((len(mask), self.lat_dim), device=self.device)
        gen_nodes = self._generate(latents, mask, ctxt)
        return gen_nodes

    def get_losses(self, sample, _batch_idx: int, _epoch_num: int) -> dict:
        """Get the losses given a sample.

        The sample is a tuple contatining the (GraphBatch, Context, Label) due
        to consistancy with the graph classification tasks
        """
        graphs, ctxt, labels = sample

        ## Pass through the entire model
        rec_nodes, _, means, lstds = self.forward(graphs, ctxt)

        ## Get the loss value for the reconstruction
        rec_loss = masked_dist_loss(
            self.gen_loss_fn,
            rec_nodes,
            graphs.mask,
            graphs.nodes,
            graphs.mask,
        )

        ## Get the loss value for the latent space
        if self.do_vae:
            if self.do_outlier_exposure:
                enc_loss = kld_with_OE(means, lstds, labels)
            else:
                enc_loss = kld_to_norm(means, lstds)
        else:
            enc_loss = T.zeros_like(rec_loss)

        ## Add these losses to the dictionary and include the total
        loss_dict = self.loss_dict_reset()  ## Ensures all defined keys are 0
        loss_dict["lat_loss"] = enc_loss
        loss_dict["gen_loss"] = rec_loss
        loss_dict["total"] = enc_loss * self.kld_weight + rec_loss

        ## Step the kld weight
        self._step_kld_weight()

        return loss_dict

    def visualise(
        self,
        loader: DataLoader,
        path: str = "SetMePlease!",
        flag: str = "",
        epochs: int = 0,
    ) -> None:
        """A visualisation function for the TransformerVAE
        - Plots the distributions of the latent space marginals
        - Plots the reconstructed distributions

        Does so for 5000 graphs
        """

        ## Get 5000 samples from the loader
        all_means = []
        all_latents = []
        all_labels = []
        all_tru_nodes = []
        all_rec_nodes = []
        all_mask = []
        count = 0
        for graphs, ctxt, labels in loader:

            ## Pass through the network with no loss tracking
            rec_nodes, latents, means, _ = self.forward(graphs.to(self.device))

            ## Add to the lists
            all_means.append(means)
            all_latents.append(latents)
            all_labels.append(labels)

            ## Save the nodes and mask
            all_tru_nodes.append(graphs.nodes)
            all_rec_nodes.append(rec_nodes)
            all_mask.append(graphs.mask)

            ## Break loop at 5000
            count += len(labels)
            if count > 5000:
                break

        ## Concatenate and convert to numpy
        all_means = to_np(T.cat(all_means))
        all_latents = to_np(T.cat(all_latents))
        all_labels = to_np(T.cat(all_labels))
        all_tru_nodes = to_np(T.cat(all_tru_nodes))
        all_rec_nodes = to_np(T.cat(all_rec_nodes))
        all_mask = to_np(T.cat(all_mask))

        ## Calculate the mass and pt from the nodes
        coords = loader.dataset.dataset.coordinates.node

        if coords == ["del_eta", "del_phi", "log_pt"]:
            all_tru_nodes[..., -1] = np.exp(all_tru_nodes[..., -1])
            all_rec_nodes[..., -1] = np.exp(all_rec_nodes[..., -1])
            all_tru_mass = cstetaphipt_to_jetmass(all_tru_nodes, all_mask)
            all_rec_mass = cstetaphipt_to_jetmass(all_rec_nodes, all_mask)
            all_tru_pt = np.sum(all_tru_nodes[..., -1] * all_mask, axis=-1)
            all_rec_pt = np.sum(all_rec_nodes[..., -1] * all_mask, axis=-1)
        elif coords == ["del_eta", "del_phi", "pt"]:
            all_tru_mass = cstetaphipt_to_jetmass(all_tru_nodes, all_mask)
            all_rec_mass = cstetaphipt_to_jetmass(all_rec_nodes, all_mask)
            all_tru_pt = np.sum(all_tru_nodes[..., -1] * all_mask, axis=-1)
            all_rec_pt = np.sum(all_rec_nodes[..., -1] * all_mask, axis=-1)
        elif coords == ["px", "py", "pz"]:
            all_tru_mass = cstpxpypz_to_jetmass(all_tru_nodes, all_mask)
            all_rec_mass = cstpxpypz_to_jetmass(all_rec_nodes, all_mask)
            all_tru_pt = np.zeros_like(all_tru_mass)
            all_rec_pt = np.zeros_like(all_tru_mass)

        ## Log squash the node data for the visualisations
        all_tru_nodes = log_squash(all_tru_nodes)
        all_rec_nodes = log_squash(all_rec_nodes)

        ## Plot the latent space distribution of the means
        fig = plot_latent_space(
            path / ("latent_means_" + flag),
            all_means,
            all_labels,
            loader.dataset.dataset.n_classes,
            return_fig=True,
        )
        if wandb.run is not None:
            wandb.log({"latent_means": wandb.Image(fig)}, commit=False)

        ## Plot the latent space distribution of the samples
        fig = plot_latent_space(
            path / ("latent_samples_" + flag),
            all_latents,
            all_labels,
            loader.dataset.dataset.n_classes,
            return_fig=True,
        )
        if wandb.run is not None:
            wandb.log({"latent_samples": wandb.Image(fig)}, commit=False)

        ## Plot the distributions of the jet pts
        fig = plot_multi_hists(
            path / ("pts_" + flag),
            [all_tru_pt[..., None], all_rec_pt[..., None]],
            ["Originals", "Reconstructions"],
            ["pts"],
            return_fig=True,
        )
        if wandb.run is not None:
            wandb.log({"pts": wandb.Image(fig)}, commit=False)

        ## Plot the distributions of the jet masses
        fig = plot_multi_hists(
            path / ("masses_" + flag),
            [all_tru_mass[..., None], all_rec_mass[..., None]],
            ["Originals", "Reconstructions"],
            ["masses"],
            return_fig=True,
        )
        if wandb.run is not None:
            wandb.log({"masses": wandb.Image(fig)}, commit=False)

        ## Plot the distributions of the inputs and output node variables
        fig = plot_multi_hists(
            path / ("nodes_" + flag),
            [all_tru_nodes[all_mask], all_rec_nodes[all_mask]],
            ["Originals", "Reconstructions"],
            loader.dataset.dataset.coordinates["node"],
            return_fig=True,
        )
        if wandb.run is not None:
            wandb.log({"nodes": wandb.Image(fig)}, commit=False)

        ## Plot the scatter plots for the first four samples
        graphs, ctxt, labels = gcoll([loader.dataset[idx] for idx in [0, 1, -1, -2]])
        if graphs.nodes.shape[-1] in [2, 3]:
            rec_nodes = self.forward(graphs.to(self.device))[0]
            fig = plot_scatter(
                path / ("scatter_" + flag),
                to_np(graphs.nodes),
                to_np(graphs.mask),
                to_np(rec_nodes),
                to_np(graphs.mask),
                return_fig=True,
            )
            if wandb.run is not None:
                wandb.log({"scatter": wandb.Image(fig)}, commit=False)
        plt.close("all")
