"""
Functions relating to the loading and manipulation of jet tensors
"""

from heapq import merge
from pathlib import Path
import random
from typing import Tuple
from copy import deepcopy

import numpy as np
import pandas as pd

from jutils.data_loading import read_all_data
from mattstools.utils import log_squash, min_loc

EPS = 1e-8  ## epsilon for preventing division by zero
global EDGE_COMPRESS


def get_eng_from_ptetaphiM(particle):
    """Given a 3 or 4 vector (with mass) in ptetaphi space, calculate the energy"""
    pt = particle[..., 0]
    eta = particle[..., 1]

    ## Extra step if mass is present
    if particle.shape[-1] > 3:
        mass = particle[..., 3]
        return np.sqrt(mass**2 + (pt * np.cosh(eta)) ** 2)

    return pt * np.cosh(eta)


def cartesian_to_ptetaphi(
    px: np.ndarray, py: np.ndarray = None, pz: np.ndarray = None
) -> np.ndarray:
    """Convert from cartesian to ATLAS co-ordinates"""

    ## If only one argument is given then do manual splitting
    if py is None and pz is None:
        py = px[..., 1]
        pz = px[..., 2]
        px = px[..., 0]

    pt = np.sqrt(px**2 + py**2)
    mtm = np.sqrt(px**2 + py**2 + pz**2)
    eta = np.arctanh(np.clip(pz / (mtm + EPS), -1 + EPS, 1 - EPS))
    phi = np.arctan2(py, px)

    return pt, eta, phi


def ptetaphi_to_cartesian(
    pt: np.ndarray, eta: np.ndarray = None, phi: np.ndarray = None
) -> np.ndarray:
    """Convert from ATLAS to cartesian co-ordinates"""

    ## If only one argument is given then do manual splitting
    if eta is None and phi is None:
        eta = pt[..., 1]
        phi = pt[..., 2]
        pt = pt[..., 0]

    px = pt * np.cos(phi)
    py = pt * np.sin(phi)
    pz = pt * np.sinh(eta)

    return px, py, pz


def get_boost_from_spherical(
    pt: np.ndarray, eta: np.ndarray = None, phi: np.ndarray = None, mass: np.ndarray = 0
) -> np.ndarray:
    """Return the appropriate boost vector in cartesian coordinates given a 4 momentum
    defined by pt, eta, phi, and optionally mass
    """

    ## If one arrgument assume it contains all
    if eta is None and phi is None:
        mass = mass if pt.shape[-1] < 4 else pt[..., -1]
        eta = pt[..., 1]
        phi = pt[..., 2]
        pt = pt[..., 0]
    denom = np.sqrt(mass**2 + (pt * np.cosh(eta)) ** 2)
    boost = np.vstack(ptetaphi_to_cartesian(pt, eta, phi)).T / (denom + 1e-8)
    return np.squeeze(boost)


def apply_boost(vector: np.ndarray, boost: np.ndarray) -> np.ndarray:
    """Boosts a 4 vector into the frame given by a 3 vector
    - Opposite of ROOT::TLorentzBoost!
    - Returns a relativistic vector-boost
    - The vector can be batched, only one boost vector though!
    args:
        vector: The vector being boosted as E/c, x, y, z
        boost: The boost vector as x, y, z
    """
    ## Ensure boost is flat
    boost = np.squeeze(boost)

    ## Calculate the beta/gamma coeff (some jets have 1 const cant boost that much!)
    beta2 = np.clip(np.sum(boost**2), 1e-8, 1 - 1e-8)
    gamma = 1 / np.sqrt(1 - beta2)

    ## Create the boost matrix
    lmb = np.zeros([4, 4])
    lmb[1:, 1:] = np.expand_dims(boost, -2) * np.expand_dims(boost, -1) / beta2
    lmb[1:, 1:] *= gamma - 1
    lmb[1:, 1:] += np.diag([1, 1, 1])
    lmb[0, 1:] = -boost * gamma
    lmb[1:, 0] = -boost * gamma
    lmb[0, 0] = gamma

    ## Boost the vector using batched matrix multiplication
    return np.einsum("ij,bj->bi", lmb, vector)


def boost_jet_mopt(cnsts: np.ndarray, jets: np.ndarray, mopt: float = 1.0) -> tuple:
    """Boost a jet and its constituents along the jet axis until the m/pt of the jet
    reaches a pre-defined value.

    This is done by boosting the entire jet first into its reference frame,
    then boosting again to a specific mass over pt value

    args:
        cnsts: The kinematics of the jet constituents (pt, eta, phi)
        jets: The kinematics of the jet (pt, eta, phi, mass)
        mask: Boolean array showing padded level of the constituents
    kwargs:
        mopt: The desired mass over pt for the output jet
        random: Randomly boost the jet using the mopt as the upper bound
    """

    ## We need the cartesian representations the constituent momenta
    cst_px, cst_py, cst_pz = ptetaphi_to_cartesian(cnsts)
    cst_e = cnsts[..., 0] * np.cosh(cnsts[..., 1])  # From pt and eta
    cnsts = np.stack([cst_e, cst_px, cst_py, cst_pz], axis=1)

    ## The first boosting vector comes from the jet's velocity (divide p by e)!
    first_boost = get_boost_from_spherical(jets)
    cnsts = apply_boost(cnsts, first_boost)

    ## If we want that reference frame then we leave it
    if mopt == -1:
        cnsts = np.stack(cartesian_to_ptetaphi(cnsts[..., 1:]), axis=1).astype("f")
        jets[..., 0] = 0
        return cnsts, jets

    ## The second boosting vector is derived from a specific mopt value (negative)
    second_boost = -get_boost_from_spherical(
        jets[..., 3] / mopt, jets[..., 1], jets[..., 2], jets[..., 3]
    )
    cnsts = apply_boost(cnsts, second_boost)

    ## Get back to pt eta phi
    cnsts = np.stack(cartesian_to_ptetaphi(cnsts[..., 1:]), axis=1).astype("f")
    jets[..., 0] = jets[..., -1] / mopt
    return cnsts, jets


def empty_0dim_like(arr: np.ndarray) -> np.ndarray:
    """Returns an empty array with similar size as the input but with its final
    dimension size reduced to 0
    """

    ## Get all but the final dimension
    all_but_last = arr.shape[:-1]

    ## Ensure that this is a tuple/list so it can agree with return syntax
    if isinstance(all_but_last, int):
        all_but_last = [all_but_last]

    return np.empty((*all_but_last, 0), dtype=arr.dtype)


def signed_angle_diff(angle1, angle2):
    """Calculate diff between two angles reduced to the interval of [-pi, pi]"""
    return (angle1 - angle2 + np.pi) % (2 * np.pi) - np.pi


def inv_mass_all_pairs(cnsts: np.ndarray) -> np.ndarray:
    """Return the invariant mass matrix of all pairs of constuents"""

    ## We need the cartesian representations the constituent momenta
    cst_px, cst_py, cst_pz = ptetaphi_to_cartesian(cnsts)
    cst_e = cnsts[..., 0] * np.cosh(cnsts[..., 1])  # From pt and eta

    ## Get the expanded sum of all pairs
    cst_px = np.expand_dims(cst_px, -1) + np.expand_dims(cst_px, -2)
    cst_py = np.expand_dims(cst_py, -1) + np.expand_dims(cst_py, -2)
    cst_pz = np.expand_dims(cst_pz, -1) + np.expand_dims(cst_pz, -2)
    cst_e = np.expand_dims(cst_e, -1) + np.expand_dims(cst_e, -2)

    ## Return the mass from p_mu^2
    return np.sqrt(
        np.clip(cst_e**2 - cst_px**2 - cst_py**2 - cst_pz**2, 1e-8, None)
    )


def load_toptag(
    dset: str = "test",
    path: str = "/srv/beegfs/scratch/groups/rodem/anomalous_jets/virtual_data/",
    datasets: dict = None,
    n_jets: int = -1,
    n_csts: int = -1,
    min_n_csts: int = 0,
) -> tuple:
    """Load jets and constituents from the toptagging reference dataset and return them
    as numpy arrays defined by:
    - pt, eta, phi, mass

    kwargs:
        dset: Either train, test, or val
        path: The location folder which contains the train, test HDF files
        datasets: dict containing which processes to load
        n_jets: The number of jets to load, split for each process (-1 = load all)
        n_csts: The number of constituents to load per jet (can be zero)

    returns:
        jet_data: High level jets variables [pt, eta, phi, M, *substruc]
        cst_data: Constituent vairables [pt, eta, phi]
            empty if n_cnsts is 0
        mask: A boolean array showing the real vs padded elements of the cst data
            empty if n_cnsts is 0
        labels: Jet class variables
            Class label is based on order of datasets list
            min_n_csts: Minimum number of constituents per jet
    """

    ## Default processes to load
    datasets = datasets or {"c1": ["QCD"]}
    procs = []
    for _, prc in datasets.items():
        for p in prc:
            procs.append(p)
            if p not in ["QCD", "ttbar"]:
                raise ValueError(f"Unknown process for toptag jets: {p}")

    print(f"Loading {n_jets} jets from the {dset} set in the TopTagging ref. dataset")
    print(f" -- processes selected: {datasets}")

    ## Load the relevant file using pandas
    n_jets = n_jets if n_jets != -1 else None
    cst_data = pd.read_hdf(Path(path, dset + ".h5"), "table", stop=n_jets)

    ## Pull out the class labels
    labels = cst_data.is_signal_new.to_numpy()

    ## Trim the file based on the requested process then the number of jets
    selection = (("QCD" in procs) & (labels == 0)) | (
        ("ttbar" in procs) & (labels == 1)
    )
    cst_data = cst_data[selection][:n_jets]
    labels = labels[selection][:n_jets]

    ## Select the constituent columns columns
    col_names = ["E", "PX", "PY", "PZ"]
    cst_cols = [f"{var}_{i}" for i in range(200) for var in col_names]
    cst_data = np.reshape(
        cst_data[cst_cols].to_numpy().astype(np.float32), (-1, 200, 4)
    )

    ## Filter out events with too few constituents
    if min_n_csts > 0:
        min_mask = np.sum(cst_data[..., 0] > 0, axis=-1) >= min_n_csts
        cst_data = cst_data[min_mask]

    ## Splitting in this way does not result in any memory copy
    cst_e = cst_data[..., 0:1]
    cst_px = cst_data[..., 1:2]
    cst_py = cst_data[..., 2:3]
    cst_pz = cst_data[..., 3:4]

    ## Calculate the overall jet kinematics from the constituents
    jet_px, jet_py, jet_pz, jet_m, _ = constituents_to_jet(
        cst_px, cst_py, cst_pz, cst_e
    )

    ## Limit constituent data to the number of requested nodes
    cst_px = cst_px[:, :n_csts]
    cst_py = cst_py[:, :n_csts]
    cst_pz = cst_pz[:, :n_csts]

    ## Convert both sets of values to spherical
    cst_pt, cst_eta, cst_phi = cartesian_to_ptetaphi(cst_px, cst_py, cst_pz)
    jet_pt, jet_eta, jet_phi = cartesian_to_ptetaphi(jet_px, jet_py, jet_pz)

    ## Combine the information and return
    cst_data = np.concatenate([cst_pt, cst_eta, cst_phi], axis=-1)
    jet_data = np.vstack([jet_pt, jet_eta, jet_phi, jet_m]).T

    ## Get the mask from the cst pt
    mask = np.squeeze(cst_pt > 0)

    return jet_data, cst_data, mask, labels


def load_rodem(
    dset: str = "test",
    path: str = "/srv/beegfs/scratch/groups/rodem/anomalous_jets/virtual_data/",
    datasets: dict = None,
    leading: bool = True,
    incl_substruc: bool = False,
    n_jets: int = -1,
    n_csts: int = -1,
    min_n_csts: int = 0,
) -> tuple:
    """Load jets and constituents from the rodem's virtual dataset and return them as
    numpy arrays defined by:
    - pt, eta, phi, mass (and extra vars for jets)

    kwargs:
        dset: Either train or test (validation splitting is done on your own)
        path: The location folder which contains the train, test virtual HDF files
        datasets: dict containing which processes to load
        n_jets: The number of jets to load, split for each process (-1 = load all)
        n_csts: The number of constituents to load per jet (can be zero)
        procs: A list of processes to pull from [QCD, ttbar, W]
        incl_substruc: If the jet substructure vars should be in the high lvl array

    returns:
        jet_data: High level jets variables [pt, eta, phi, M, *substruc]
        cst_data: Constituent vairables [pt, eta, phi]
            empty if n_cnsts is 0
        mask: A boolean array showing the real vs padded elements of the cst data
            empty if n_cnsts is 0
        labels: Jet class variables
            Class label is based on order of datasets list
        min_n_csts: Minimum number of constituents per jet
    """
    print(f"Loading {n_jets} jets from the {dset} set in the RODEM dataset")
    print(f" -- processes selected: {datasets}")

    ## Default dict
    datasets = deepcopy(datasets) or {"c1": ["QCD"]}

    ## Cycle through the dataset dict and make the full names
    pt_range = "550_650_" if "restricted_data" in path else "450_1200_"
    full_names = {
        "QCD": "QCD_jj_pt_" + pt_range,
        "ttbar": "ttbar_allhad_pt_" + pt_range,
        "WZ": "WZ_jjnunu_pT_" + pt_range,
        "H2": "H2tbtb_1800_HC_400_",
    }
    for k, prcs in datasets.items():
        for i, p in enumerate(prcs):
            if p not in ["QCD", "ttbar", "WZ", "H2"]:
                raise ValueError(f"Unknown process for RODEM jets: {p}")
            datasets[k][i] = str(Path(path, full_names[p] + dset + ".h5"))

    ## Read in data for each as numpy arrays
    data = read_all_data(
        config={
            "data_sets": datasets,
            "data_type": {
                "info": "all",
                "n_jets": n_jets,
                "n_cnsts": n_csts,
                "leading": leading,
                "incl_substruc": incl_substruc,
                "astype": "float32",
            },
        }
    )

    ## Decode the results and put them into single tensors
    jet_data = np.concatenate([prcs[0] for _, prcs in data.items()])
    cst_data = np.concatenate([prcs[1][..., :3] for _, prcs in data.items()])  ## No M

    ## Labels are based on the ordering of the datasets dictionary
    labels = (
        np.concatenate(
            [len(cls[0]) * [cls_i] for cls_i, (_, cls) in enumerate(data.items())]
        )
        .flatten()
        .astype(np.long)
    )

    ## Filter out events with too few constituents
    if min_n_csts > 0:
        min_mask = np.sum(cst_data[..., 0] > 0, axis=-1) >= min_n_csts
        cst_data = cst_data[min_mask]
        jet_data = jet_data[min_mask]
        labels = labels[min_mask]

    ## Get the mask based on pt
    mask = cst_data[..., 0] > 0

    ## Clip the jet masses due to small error in RODEM data
    jet_data[..., 3:4] = np.clip(jet_data[..., 3:4], a_min=0, a_max=None)

    return jet_data, cst_data, mask, labels


def build_jet_edges(
    nodes: np.ndarray, mask: np.ndarray, coords: list, delR_threshold: float = 0
) -> Tuple[np.ndarray, np.ndarray]:
    """Build the jet edges based on the current node features

    args:
        nodes: Low level constituent information, pt, eta, phi
        high: High level jet information, pt, eta, phi, M, other
        mask: A boolean tensor showing which cnsts are real vs padded
        coords: A list of strings containing the requested edge features
    kwargs:
        delR_threshold: The limit to connect two nodes, if 0 no edges are made at all
    """
    n_nodes = len(nodes)

    ## If we want fully connects with no edge features then return so
    if delR_threshold > 100 and not (coords):
        edges = np.empty((n_nodes * n_nodes, 0), dtype="f")
        adjmat = np.ones((n_nodes, n_nodes), dtype="bool")
        return edges, adjmat

    ## If the del r value is zero then the edges will be empty
    if delR_threshold == 0:
        edges = np.empty((0, 0), dtype="f")
        adjmat = np.zeros((n_nodes, n_nodes), dtype="bool")
        return edges, adjmat

    ## Create a padding matrix of possible connections between real nodes
    pad_mask = np.expand_dims(mask, -1) * np.expand_dims(mask, -2)

    ## Create get the spacial seperations between nodes
    pt = nodes[..., 0:1]
    eta = nodes[..., 1:2]
    phi = nodes[..., 2:3]
    del_eta = np.expand_dims(eta, -2) - np.expand_dims(eta, -3)
    del_phi = signed_angle_diff(np.expand_dims(phi, -2), np.expand_dims(phi, -3))

    ## Create the delR matrix between each node
    delR_matrix = np.sqrt(del_eta**2 + del_phi**2)

    ## Build the adjacency matrix where the distances fall below the threshold
    adjmat = np.squeeze(delR_matrix < delR_threshold)
    adjmat *= ~np.identity(len(adjmat), dtype="bool")  ## Remove self connections
    adjmat[~pad_mask] = 0  ## Make sure the padded elements are false

    ## Start building the dictionary of lambda functions
    edges = {
        "del_eta": lambda: del_eta,
        "del_phi": lambda: del_phi,
        "del_R": lambda: delR_matrix,
        "pt_send": lambda: np.expand_dims(pt * adjmat, -1),
        "pt_sum": lambda: np.expand_dims(pt, -2) + np.expand_dims(pt, -3),
    }

    ## Extra variables coming from lambda functions
    edges["log_kt"] = lambda: np.log(
        np.clip(edges["pt_send"]() * edges["del_R"](), 1e-8, None)
    )
    edges["z"] = lambda: edges["pt_send"]() / (edges["pt_sum"]() + 1e-8)
    edges["log_m"] = lambda: np.clip(
        np.log(np.expand_dims(inv_mass_all_pairs(nodes), -1)), -5, None
    )
    edges["psi"] = lambda: np.arctan2(del_eta, del_phi)
    edges["log_del_R"] = lambda: np.log(np.clip(delR_matrix, 1e-5, None))

    ## Select the appropriate coordinates
    if len(coords) > 0:
        edges = np.concatenate([edges[key]()[adjmat] for key in coords], axis=-1)
    else:
        edges = np.empty((0, 0), dtype="f")
    return edges, adjmat


def graph_coordinates(
    raw_nodes: np.ndarray,
    raw_high: np.ndarray,
    mask: np.ndarray,
    coordinates: dict,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:
    """Converts the standard ATLAS 4 vector to a specific set of coordinates
    for use in a graph network
     - The vectors are of the format pt, eta, phi, (M, other for jets)
     - Only the kinematics and mass variables are transformed/dropped
     - The "other" variables will always be in the final array

    args:
        raw_nodes: Low level constituent information, pt, eta, phi
        raw_high: High level jet information, pt, eta, phi, M, other
        mask: A boolean tensor showing which cnsts are real vs padded
        coordinates: A dict with keys for edge, node, high coordinates
    """

    ## Extract the individual measurements from the underlying tensors
    ## Splitting in this way does not result in any memory copy
    jet_pt = raw_high[..., 0:1]
    jet_eta = raw_high[..., 1:2]
    jet_phi = raw_high[..., 2:3]
    jet_mass = raw_high[..., 3:4]
    jet_oth = raw_high[..., 4:] if raw_high.shape[-1] > 4 else empty_0dim_like(jet_mass)
    cst_pt = raw_nodes[..., 0:1]
    cst_eta = raw_nodes[..., 1:2]
    cst_phi = raw_nodes[..., 2:3]
    cst_oth = (
        raw_nodes[..., 3:] if raw_nodes.shape[-1] > 3 else empty_0dim_like(cst_phi)
    )

    ## Put all the jet tensors into a single dictionary
    high = {
        "pt": lambda: jet_pt,
        "eta": lambda: jet_eta,
        "phi": lambda: jet_phi,
        "mass": lambda: jet_mass,
        "eng": lambda: np.sqrt(jet_mass**2 + (jet_pt * np.cosh(jet_eta)) ** 2),
        "px": lambda: jet_pt * np.cos(jet_phi),
        "py": lambda: jet_pt * np.sin(jet_phi),
        "pz": lambda: jet_pt * np.sinh(jet_eta),
        "log_pt": lambda: np.log(jet_pt + 1e-8),
        "log_mass": lambda: np.log(jet_mass + 1e-8),
        "mopt": lambda: jet_mass / (jet_pt + 1e-8),
    }

    ## Put all of the constituent tensors into a single dictionary
    nodes = {
        "pt": lambda: cst_pt,
        "eta": lambda: cst_eta,
        "phi": lambda: cst_phi,
        "eng": lambda: cst_pt * np.cosh(cst_eta),
        "px": lambda: cst_pt * np.cos(cst_phi),
        "py": lambda: cst_pt * np.sin(cst_phi),
        "pz": lambda: cst_pt * np.sinh(cst_eta),
        "del_eta": lambda: cst_eta - np.expand_dims(jet_eta, -1),
        "del_phi": lambda: signed_angle_diff(cst_phi, np.expand_dims(jet_phi, -1)),
        "log_pt": lambda: np.log(cst_pt + 1e-8),
        "pt_frac": lambda: cst_pt / (np.expand_dims(jet_pt, -1) + 1e-8),
        "logsquash_pt": lambda: log_squash(cst_pt),
    }

    ## Other derived variables require the dicts to already be created
    high["log_eng"] = lambda: np.log(high["eng"]() + 1e-8)
    nodes["log_eng"] = lambda: np.log(nodes["eng"]() + 1e-8)
    nodes["log_eng_frac"] = lambda: nodes["log_eng"]() - np.expand_dims(
        high["log_eng"](), -1
    )
    nodes["log_pt_frac"] = lambda: nodes["log_pt"]() - np.expand_dims(
        high["log_pt"](), -1
    )
    nodes["pt_frac_pc"] = lambda: nodes["pt"]() / (np.sum(nodes["pt"]()) + 1e-8)
    nodes["log_pt_frac_pc"] = lambda: np.log(nodes["pt_frac_pc"]() + 1e-8)
    nodes["del_R"] = lambda: np.sqrt(nodes["del_eta"]() ** 2 + nodes["del_phi"]() ** 2)

    ## Combine the list of chosen arguments with the other information if present
    nodes = np.concatenate(
        [nodes[key]() for key in coordinates["node"]] + [cst_oth], axis=-1
    )
    high = np.concatenate(
        [high[key]() for key in coordinates["high"]] + [jet_oth], axis=-1
    )

    ## Ensure nodes still adheres to mask!
    ## This is because the deltas and ratios will no longer be 0!
    nodes[~mask] = 0.0

    return nodes, high


def constituents_to_jet(
    cst_px: np.ndarray, cst_py: np.ndarray, cst_pz: np.ndarray, cst_e: np.ndarray = None
) -> np.ndarray:
    """Calculate high level jet variables using only the constituents
    args:
        cst_px: The constituent px
        cst_py: The constituent py
        cst_pz: The constituent pz
    kwargs:
        cst_e: The constituent E to calculate total jet energy
               If none then cst are assumed to be massless and energy = momentum
    """

    ## Calculate the total jet momenta
    jet_px = np.squeeze(cst_px).sum(axis=-1)
    jet_py = np.squeeze(cst_py).sum(axis=-1)
    jet_pz = np.squeeze(cst_pz).sum(axis=-1)

    ## Calculate the total jet energy
    if cst_e is None:
        cst_e = np.sqrt(cst_px**2 + cst_py**2 + cst_pz**2)
    jet_e = np.squeeze(cst_e).sum(axis=-1)

    ## Calculate the total jet mass
    jet_m = np.sqrt(np.maximum(jet_e**2 - jet_px**2 - jet_py**2 - jet_pz**2, 0))

    return jet_px, jet_py, jet_pz, jet_m, jet_e


def locals_to_jet(nodes: np.ndarray, mask: np.ndarray, jets: np.ndarray) -> np.ndarray:
    """Calculate the overall jet kinematics using only the local info:
    - del_eta
    - del_phi
    - pt_fraction
    """

    ## Calculate the constituent pt, eta and phi
    eta = nodes[..., 0] + jets[:, 1:2]
    phi = nodes[..., 1] + jets[:, 2:3]
    # pt = np.exp(nodes[..., 2])
    # pt[~mask] = 0
    # pt /= np.sum(pt * mask, axis=-1, keepdims=True)
    pt = nodes[..., 2] * jets[:, 0:1]

    ## Calculate the total jet values (always include the mask when summing!)
    jet_px = (pt * np.cos(phi) * mask).sum(axis=-1)
    jet_py = (pt * np.sin(phi) * mask).sum(axis=-1)
    jet_pz = (pt * np.sinh(eta) * mask).sum(axis=-1)
    jet_e = (pt * np.cosh(eta) * mask).sum(axis=-1)

    ## Get the derived jet values, the clamps ensure NaNs dont occur
    jet_pt = np.sqrt(jet_px**2 + jet_py**2)
    # jet_eta = np.arctanh(np.clip(jet_pz/jet_e, EPS-1, 1-EPS))
    # jet_phi = np.arctan2(jet_py, jet_px)
    jet_m = np.sqrt(np.clip(jet_e**2 - jet_pt**2 - jet_pz**2, EPS, None))

    return np.vstack([jet_pt, jet_m]).T


def cstpxpypz_to_jetmass(nodes: np.ndarray, mask: np.ndarray) -> np.ndarray:
    """Calculate the overall jet kinematics using the constituent cartesian info
    - px, py, pz
    """

    ## Break up the constituent coordinates
    px = nodes[..., 0]
    py = nodes[..., 1]
    pz = nodes[..., 2]
    e = np.sqrt(np.clip(px**2 + py**2 + pz**2, EPS, None))

    ## Calculate the total jet values (always include the mask when summing!)
    jet_px = (px * mask).sum(axis=-1)
    jet_py = (py * mask).sum(axis=-1)
    jet_pz = (pz * mask).sum(axis=-1)
    jet_e = (e * mask).sum(axis=-1)

    ## Calculate the jet mass from its 4 momenta
    jet_p2 = jet_px**2 + jet_py**2 + jet_pz**2
    jet_m = np.sqrt(np.clip(jet_e**2 - jet_p2, EPS, None))

    return jet_m


def cstetaphipt_to_jetmass(nodes: np.ndarray, mask: np.ndarray) -> np.ndarray:
    """Calculate the overall jet kinematics using the constituent cartesian info
    - eta, phi, pt
    """

    ## Break up the coordinates
    eta = nodes[..., 0].copy()
    phi = nodes[..., 1].copy()
    pt = nodes[..., 2].copy()

    ## Calculate the constituent px, py, pz
    pxpypz = np.zeros_like(nodes)
    pxpypz[..., 0] = pt * np.cos(phi)
    pxpypz[..., 1] = pt * np.sin(phi)
    pxpypz[..., 2] = pt * np.sinh(eta)

    return cstpxpypz_to_jetmass(pxpypz, mask)


def crop_constituents(csts, mask, amount=10, min_allowed=10):
    """Drop the last 'amount' of constituents in the jet.

    Will not drop constituents passed the minimum allowed level
    """

    ## First check how many we can drop
    n_csts = np.sum(mask)
    allowed_to_drop = min(n_csts - min_allowed, amount)

    ## Generate randomly the number of nodes to kill if allowed to drop
    if allowed_to_drop > 0:
        drop_num = np.random.randint(allowed_to_drop + 1)

        ## Drop them from the mask and the node features
        mask[n_csts - drop_num : n_csts] = False
        csts[n_csts - drop_num : n_csts] = 0

    return csts, mask


def smear_constituents(csts, mask, strength=0.1, pt_min=0.5) -> np.ndarray:
    """Add noise to the constituents eta and phi to simulates soft emmisions

    Noise is gaussian with mean = 0 and deviation = strength/pT
    The default strength for the blurr is set to 100 MeV
    The default minimum smearing value is 500 MeV
    - https://arxiv.org/pdf/2108.04253.pdf
    """

    pt = np.clip(csts[mask, 0:1], a_min=pt_min, a_max=None)
    smear = np.random.randn(len(pt), 2) * strength / pt
    csts[mask, 1:] += smear

    return csts


def cambridge_aachen(
    csts: np.ndarray, mask: np.ndarray, min_number=10
) -> Tuple[np.ndarray, np.ndarray]:
    """Runs the CA clustering algorithm over the jet until a minimum crietia is reached
    This process takes some time
    """

    ## Constituents start off as massless, but by this process they gain mass
    mass = np.zeros((len(csts), 1))
    csts = np.concatenate([csts, mass], axis=-1)

    while True:

        ## Test the exit condition
        if mask.sum() <= min_number:
            break

        ## Break up the constituent coordinates
        pt = csts[..., 0]
        eta = csts[..., 1]
        phi = csts[..., 2]
        mass = csts[..., 3]

        ## Get the seperation between the nodes
        del_eta = np.expand_dims(eta, -1) - np.expand_dims(eta, -2)
        del_phi = signed_angle_diff(np.expand_dims(phi, -1), np.expand_dims(phi, -2))
        delR_matrix = np.sqrt(del_eta**2 + del_phi**2)

        ## Make sure that the fake nodes are considered too far away
        pad_mask = np.expand_dims(mask, -1) * np.expand_dims(mask, -2)
        delR_matrix[~pad_mask] = np.inf

        ## Make the diagonal large, can't merge a node with itself
        np.fill_diagonal(delR_matrix, np.inf)

        ## Check to see if there are any viable merge candidates
        if np.isinf(np.min(delR_matrix)):
            break

        ## Select the two closest constituents
        i, j = min_loc(delR_matrix)
        sel_csts = csts[[i, j]]
        sel_pt = sel_csts[..., 0]
        sel_eta = sel_csts[..., 1]
        sel_phi = sel_csts[..., 2]
        sel_mass = sel_csts[..., 3]

        ## Convert to cartesian 4 vector
        px = sel_pt * np.cos(sel_phi)
        py = sel_pt * np.sin(sel_phi)
        pz = sel_pt * np.sinh(sel_eta)
        e = np.sqrt(np.clip(px**2 + py**2 + pz**2 + sel_mass**2, EPS, None))

        ## Get the cartesian 4 vector for the combined system of the two particles
        comb_px = px.sum()
        comb_py = py.sum()
        comb_pz = pz.sum()
        comb_p2 = comb_px**2 + comb_py**2 + comb_pz**2

        ## Replace element i (pt, eta, phi, mass refer back to original csts ndarray)
        pt[i] = np.sqrt(comb_px**2 + comb_py**2)
        eta[i] = np.arctanh(
            np.clip(comb_pz / (np.sqrt(comb_p2) + EPS), -1 + EPS, 1 - EPS)
        )
        phi[i] = np.arctan2(comb_py, comb_px)
        mass[i] = np.sqrt(np.clip(e.sum() ** 2 - comb_p2, EPS, None))

        ## Remove element j
        csts[j] = 0
        mask[j] = False

    ## Sort the constituents to take advantage of the smaller padding
    csts = csts[csts[:, 0].argsort()[::-1]][:min_number]
    mask = mask[mask.argsort()][::-1][:min_number]

    return csts, mask


def collinear_splitting(csts, mask, max_splits=20, min_pt_spit=0) -> np.ndarray:
    """Split some of the constituents into a pair ontop of each other

    In current form this function results in a array which is NOT pt ordered
    Will not split to exceed the padding limit

    kwargs:
        max_splits: Max number of splits allowed
        min_pt_spit: Not allowed to split a particle with less than this pt
    """

    ## See how many constituents can be split
    n_csts = np.sum(mask)
    n_splittable = np.sum(csts[:, 0] > min_pt_spit)
    n_to_split = min([max_splits, len(csts) - n_csts, n_splittable])
    n_to_split = np.random.randint(n_to_split + 1)

    ## If splitting will take place
    if n_to_split > 0:

        ## Randomly choose how many to split and select the idxes of them from the jet
        idx_to_split = np.random.choice(n_splittable, n_to_split, replace=False)
        new_idxes = np.arange(n_to_split) + n_csts

        ## Generate the splitting momentum fractions from uniform [0.25, 0.75]
        frc_of_splits = np.random.rand(n_to_split) / 2 + 0.25

        ## Add new particles on the end of the array with the same values
        csts[new_idxes] = csts[idx_to_split].copy()
        csts[new_idxes, 0] *= frc_of_splits  ## Reduce the pt

        ## Subtract the pt fraction from the original locations
        csts[idx_to_split, 0] *= 1 - frc_of_splits

    ## Update the mask to reflect the new additions
    mask = csts[:, 0] > 0

    return csts, mask


def rotatate_constituents(csts, high, mask, angle=None) -> np.ndarray:
    """Rotate all constituents about the jet axis

    If angle is None it will rotate by a random amount
    """

    ## Define the rotation matrix
    angle = angle or np.random.rand() * 2 * np.pi
    c = np.cos(angle)
    s = np.sin(angle)
    rot_matrix = np.array([[c, -s], [s, c]])

    ## Apply to variables wrt the jet axis
    del_csts = np.array(
        (csts[mask, 1] - high[1], signed_angle_diff(csts[mask, 2], high[2]))
    )
    del_csts = rot_matrix.dot(del_csts).T
    del_csts += high[1:3]

    ## Modify the constituent with the new variables
    csts[mask, 1:] = del_csts

    return csts


def merge_constituents(
    csts: np.ndarray,
    mask: np.ndarray,
    max_del_r: float = 0.05,
    strength: float = 0.2,
    min_allowed: int = 10,
) -> Tuple[np.ndarray, np.ndarray]:
    """Randomly merge soft constituents if they are close enough together

    Similar to the smearing, the merge conditions is based on the pt and a strength
    parameter.

    kwargs:
        max_del_r: Maximum seperation to consider merging
        strength: The strength parameter for weighting the pt, set to 100 MeV
        min_allowed: Will not do any merging that may reduce the constituent count past
    """

    ## Count the number of constituents and how many we can drop
    num_csts = mask.sum()
    allowed_to_drop = num_csts - min_allowed
    if allowed_to_drop <= 0:
        return csts, mask

    ## Break up the constituents into their respective coordinates
    pt = csts[..., 0]
    eta = csts[..., 1]
    phi = csts[..., 2]

    ## Get the max seperation allowed by the sum pt (maker lower triangular)
    sum_pt = np.expand_dims(pt, -1) + np.expand_dims(pt, -2)
    pt_max_del_R = strength / (sum_pt + EPS)
    pt_max_del_R = np.clip(pt_max_del_R, 0, max_del_r)
    pt_max_del_R = np.tril(pt_max_del_R)
    np.fill_diagonal(pt_max_del_R, 0)

    ## Get the actual del R seperation matrix
    del_eta = np.expand_dims(eta, -1) - np.expand_dims(eta, -2)
    del_phi = signed_angle_diff(np.expand_dims(phi, -1), np.expand_dims(phi, -2))
    delR_matrix = np.sqrt(del_eta**2 + del_phi**2)

    ## Make sure that the fake nodes are considered too far away
    pad_mask = np.expand_dims(mask, -1) * np.expand_dims(mask, -2)
    delR_matrix[~pad_mask] = np.inf

    ## Look for where the delR matrix is smaller than the max (lower right)
    pos_merges = delR_matrix < pt_max_del_R

    ## Randomly kill off 50% of the merge candidates
    pos_merges[pos_merges != 0] = np.random.random(pos_merges.sum()) > 0.5
    pos_merges = np.argwhere(pos_merges)
    np.random.shuffle(pos_merges)

    ## Loop through all candidates and merge them using the 4 momenta
    previous = []
    i_idx = []  # Idxes for the results of the combine
    j_idx = []  # Idxes for the nodes to delete in the combine
    for i, j in pos_merges:
        if i not in previous and j not in previous:
            previous += [i, j]
            i_idx.append(i)
            j_idx.append(j)
            allowed_to_drop -= 1
            if allowed_to_drop <= 0:
                break

    ## Combined Momentum
    px = pt[i_idx] * np.cos(phi[i_idx]) + pt[j_idx] * np.cos(phi[j_idx])
    py = pt[i_idx] * np.sin(phi[i_idx]) + pt[j_idx] * np.sin(phi[j_idx])
    pz = pt[i_idx] * np.sinh(eta[i_idx]) + pt[j_idx] * np.sinh(eta[j_idx])

    ## Replace element i
    mtm = np.sqrt(px**2 + py**2 + pz**2)
    pt[i_idx] = np.sqrt(px**2 + py**2)
    eta[i_idx] = np.arctanh(np.clip(pz / (mtm + EPS), -1 + EPS, 1 - EPS))
    phi[i_idx] = np.arctan2(py, px)

    ## Kill element j
    csts[j_idx] = 0
    mask[j_idx] = False

    ## Sort the constituents with respect to pt again (might not be needed)
    # csts = csts[csts[:, 0].argsort()[::-1]]
    # mask = mask[mask.argsort()][::-1]

    return csts, mask


def random_boost(
    cnsts: np.ndarray, jets: np.ndarray, max_boost: float = 0.002
) -> tuple:
    """Boost a jet and its constituents along the jet axis using a random boost vector

    args:
        cnsts: The kinematics of the jet constituents (pt, eta, phi)
        jets: The kinematics of the jet (pt, eta, phi, mass)
        mask: Boolean array showing padded level of the constituents
    kwargs:
        max_boost: The maximum amount to boost the jet in GeV
    """

    ## We need the cartesian representations of the constituent momenta
    cst_px, cst_py, cst_pz = ptetaphi_to_cartesian(cnsts)
    cst_e = cnsts[..., 0] * np.cosh(cnsts[..., 1])  # From pt and eta
    cnsts = np.stack([cst_e, cst_px, cst_py, cst_pz], axis=1)

    ## The boosting direction comes from the jet direction
    boost = ptetaphi_to_cartesian(jets)
    boost = boost / np.linalg.norm(boost) * np.random.uniform(0, max_boost)

    ## Apply the boost to the constituents
    cnsts = apply_boost(cnsts, -boost)
    cnsts = np.stack(cartesian_to_ptetaphi(cnsts[..., 1:]), axis=1).astype("f")

    ## Apply the boost the jet too
    jet_m = jets[-1]
    jet_eng = get_eng_from_ptetaphiM(jets)
    boosted_jets = np.hstack([jet_eng, *ptetaphi_to_cartesian(jets)])
    boosted_jets = np.expand_dims(boosted_jets, 0)
    boosted_jets = apply_boost(boosted_jets, -boost)
    boosted_jets = cartesian_to_ptetaphi(np.squeeze(boosted_jets)[1:])
    boosted_jets = np.append(boosted_jets, jet_m)

    return cnsts.astype("f"), boosted_jets.astype("f")


def apply_augmentations(
    csts: np.ndarray,
    high: np.ndarray,
    mask: np.ndarray,
    augmentation_list: list,
    augmentation_prob: float = 1.0,
) -> tuple:
    """Apply a sequence of jet augmentations based on a list of strings

    args:
        csts: The constituent pt, eta, phi of the jet
        high: The jet pt, eta, phi, mass
        mask: A bool array showing the real vs padded nodes
        augmentation_list: A list of augmentations to apply in order to the jets
    """

    ## Iterate through each of the augmentations and apply them in turn
    for aug in augmentation_list:

        ## Check using the augmentation prob (rotation always happens!)
        if (
            aug == "rotate"
            or augmentation_prob == 1.0
            or augmentation_prob < random.random()
        ):

            ## Rotation is AWLAYS done as it does not change the physics of the jet at all
            if aug == "rotate":
                csts = rotatate_constituents(csts, high, mask)
            elif aug == "smear":
                csts = smear_constituents(csts, mask)
            elif "crop" in aug:
                csts, mask = crop_constituents(csts, mask, int(aug.split("-")[1]))
            elif "merge" in aug:
                csts, mask = merge_constituents(csts, mask, float(aug.split("-")[1]))
            elif "split" in aug:
                csts, mask = collinear_splitting(csts, mask, int(aug.split("-")[1]))
            elif "boost" in aug:
                csts, high = random_boost(
                    csts, high, max_boost=float(aug.split("-")[1])
                )
            elif "CA" in aug:
                csts, mask = cambridge_aachen(csts, mask, int(aug.split("-")[1]))

            ## Raise error for unknown augmentation
            else:
                raise ValueError(f"Unrecognised augmentation: {aug}")

    return csts, high, mask
