"""
A collection of misculaneous classes and functions useful for the training process
"""

import argparse
from typing import Tuple
from dotmap import DotMap
from mattstools.utils import get_standard_configs, args_into_conf, str2bool

EPS = 1e-8  ## epsilon for preventing division by zero


def apply_data_configs(data_conf: dict) -> dict:
    """Provide the arguments for the jet data and place within the dictionary"""

    ## Define the argument parser
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        "--n_jets",
        type=int,
        help="Number of jets to load from each file",
    )
    parser.add_argument(
        "--n_csts",
        type=int,
        help="Number of constituents to load for each jet",
    )
    parser.add_argument(
        "--augmentation_list",
        type=str,
        help="Comma seperated string for the augmentations to apply to the data",
    )
    parser.add_argument(
        "--augmentation_prob",
        type=float,
        help="Probability of applying a single augmentation in the list per sample",
    )
    parser.add_argument(
        "--del_r_edges",
        type=float,
        help="The delta R threshold to use for initial edge building",
    )
    args, _ = parser.parse_known_args()

    ## Place the arguments into the data config
    args_into_conf(args, data_conf, "n_jets")
    args_into_conf(args, data_conf, "n_csts")
    args_into_conf(args, data_conf, "augmentation_list")
    args_into_conf(args, data_conf, "augmentation_prob")
    args_into_conf(args, data_conf, "del_r_edges")

    return data_conf


def get_disc_configs(
    def_train: str = "config/classifiers/train.yaml",
    def_net: str = "config/classifiers/transformer.yaml",
    def_data: str = "config/classifiers/data.yaml",
) -> Tuple[DotMap, DotMap, DotMap]:
    """Loads, modifies, and returns three configuration dictionaries using command
    line arguments for a graph discriminator
    """

    data_conf, net_conf, train_conf = get_standard_configs(
        def_train,
        def_net,
        def_data,
    )
    data_conf = apply_data_configs(data_conf)

    ## Define the argument parser
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        "--drp",
        type=float,
        help="Dropout probability for each dense network",
    )
    parser.add_argument(
        "--depth",
        type=int,
        help="The number of GNN layers to use",
    )
    args, _ = parser.parse_known_args()

    ## Some need to be placed in exact locations
    args_into_conf(args, net_conf, "depth", "genc_kwargs/gnn_kwargs/depth")
    args_into_conf(
        args,
        net_conf,
        "drp",
        [
            "genc_kwargs/gnn_kwargs/gnb_kwargs/edge_blk_kwargs/net_kwargs/drp",
            "genc_kwargs/gnn_kwargs/gnb_kwargs/node_blk_kwargs/net_kwargs/drp",
            "genc_kwargs/gnn_kwargs/gnb_kwargs/glob_blk_kwargs/net_kwargs/drp",
            "genc_kwargs/dns_kwargs/drp",
        ],
    )

    return data_conf, net_conf, train_conf


def get_vae_configs(
    def_train: str = "config/vaes/train.yaml",
    def_net: str = "config/vaes/net.yaml",
    def_data: str = "config/vaes/data.yaml",
) -> Tuple[dict, dict, dict]:
    """Loads, modifies, and returns three configuration dictionaries using command
    line arguments for a graph autoencoder
    """

    data_conf, net_conf, train_conf = get_standard_configs(
        def_train,
        def_net,
        def_data,
    )
    data_conf = apply_data_configs(data_conf)

    ## Define the argument parser
    parser = argparse.ArgumentParser(allow_abbrev=False)
    parser.add_argument(
        "--gen_loss_name",
        type=str,
        help="Name of the loss applied to the reconstructed point clouds",
    )
    args, _ = parser.parse_known_args()

    ## Place the arguments into the configs
    args_into_conf(args, net_conf, "gen_loss_name")

    return data_conf, net_conf, train_conf
