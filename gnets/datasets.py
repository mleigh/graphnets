"""
Pytorch Dataset definitions of various collections training samples
"""

from copy import deepcopy
from pathlib import Path
from typing import List, Tuple, Union
from joblib import dump

import numpy as np

import torchvision as tv
import torch as T
import torch.nn.functional as F
from torch.utils.data import Dataset

from mattstools.utils import get_scaler, log_squash
from mattstools.plotting import plot_multi_hists
from mattstools.gnets.graphs import Graph

from .jets import (
    load_rodem,
    graph_coordinates,
    load_toptag,
    apply_augmentations,
    boost_jet_mopt,
    build_jet_edges,
)


class GraphDataset(Dataset):
    """The base class for all graph based datasets
    - All inherited classes must have well defined labels
    """

    def __init__(
        self,
        path: str = "setmepls",
        dset: str = "train",
        n_nodes: int = 20,
        n_classes: int = 0,
    ):
        """
        kwargs:
            path: The location of the datafiles
            dset: Which dataset to pull from, either train, test or valid
            n_nodes: Maximum number of nodes per sample
        """
        super().__init__()

        ## Make sure the dset variable is correct
        if dset not in ["test", "train", "val"]:
            raise ValueError("Unknown dset type: ", dset)

        ## Store the class attributes
        self.path = path
        self.dset = dset
        self.n_nodes = n_nodes
        self.n_classes = n_classes

        ## Initialise empty labels which needs to be overwritten for all child classes
        self.labels = T.empty(0)

    def __getitem__(self, idx, *args, **kwargs):
        raise NotImplementedError()

    def get_n_nodes(self):
        """Return the maximum number of nodes a sample can hold in the dataset"""
        return self.n_nodes

    def get_dim(self):
        """Return the dimension of a graph sample and its label"""
        sample, ctxt, label = self.__getitem__(0, do_preprocess=False)
        return sample.dim(), ctxt.shape[0], label.dim()

    def __len__(self):
        """Return the number of samples in the dataset using the length of the labels"""
        return len(self.labels)


class JetData(GraphDataset):
    """A pytorch dataset object containing high and low level jet information"""

    def __init__(
        self,
        dset: str,
        path: Path,
        datasets: dict,
        n_jets: int,
        n_csts: int,
        coordinates: dict,
        min_n_csts: int = 0,
        leading: bool = True,
        incl_substruc: bool = False,
        high_in_graph: str = "none",
        edge_scaler_nm: str = "none",
        node_scaler_nm: str = "none",
        high_scaler_nm: str = "none",
        del_r_edges: float = 0,
        boost_mopt: float = 0,
        augmentation_list: Union[str, List[str]] = "none",
        augmentation_prob: float = 1.0,
    ):
        """
        args:
            dset: Either train or test
            path: Path to find the jet datasets, must contain either rodem or toptag
            datasets: Which physics processes will be loaded with which labels
            n_jets: How many jets to load in the entire dataset
            n_csts: The number of constituents to load per jet
            coordinates: Dict of keys for which features to use in the graph
        kwargs:
            min_n_csts: The minimum number of constituents in each jet
                - This filter is applied after data is loaded from file so it may
                result in less jets being returned than specified.
            leading: If the leading jet should be loaded, if False subleading is loaded
            incl_substruc: If the substructure vars should be included (rodem only)
            high_in_graph: If the high level information is used in graph
                Can be either "none", "global", or "context"
            edge_scaler_nm: The name of the scaler to preprocess the edges
            cst_scaler_nm: The name of the scaler to preprocess the constituents
            jet_scaler_nm: The name of the scaler to preprocess the jets
            del_r_edges: Build and attribute graph edges using the delta R of the nodes
            boost_mopt: Boost the jet along its axis until m/pt = X
            augmentation_list: List of order of augmentations to apply during get item
            augmentation_prob: Probability of each aug in list taking effect
        """
        super().__init__(path=path, dset=dset, n_nodes=n_csts)

        ## Check arguments
        if high_in_graph not in ["none", "glob", "context"]:
            raise ValueError(f"Unrecognised jet_as_context argument: {high_in_graph}")
        if "toptag" in path:
            if incl_substruc:
                raise ValueError("Can't have substructure variables in toptag data")
            if not leading:
                raise ValueError("Toptag does not have subleading jet information")
        if boost_mopt and "boost" in augmentation_list:
            raise ValueError("Can not use boosting as preprocessing and augmentation!")
        if boost_mopt == -1:  # When boosting into the reference frame of the jet
            if any(tst in sr for tst in ["pt", "del"] for sr in coordinates["node"]):
                raise ValueError("Should only use xyz when boosting into jet frame!")

        ## Check if the augmentation list is a string, and split using commas
        if isinstance(augmentation_list, str):
            if augmentation_list == "none":
                augmentation_list = []
            elif augmentation_list == "all":
                augmentation_list = [
                    "rotate",
                    "crop-10",
                    "merge-0.05",
                    "split-10",
                    "smear",
                    "boost-0.05",
                ]
            else:
                augmentation_list = [x for x in augmentation_list.split(",") if x]
        augmentation_list = augmentation_list.copy() or []

        ## Save the name of the scaler and the variables used
        self.datasets = datasets.copy()
        self.coordinates = deepcopy(coordinates)
        self.min_n_csts = min_n_csts
        self.leading = leading
        self.incl_substruc = incl_substruc
        self.high_in_graph = high_in_graph
        self.del_r_edges = del_r_edges
        self.boost_mopt = boost_mopt
        self.augmentation_list = augmentation_list
        self.augmentation_prob = augmentation_prob
        self.do_augment = bool(augmentation_list)
        self.scalers = {
            "edge": get_scaler(edge_scaler_nm),
            "node": get_scaler(node_scaler_nm),
            "high": get_scaler(high_scaler_nm),
        }

        ## Load jets and constituents as pt, eta, phi, (M for jets)
        if "rodem" in path:
            self.high_data, self.node_data, self.mask, self.labels = load_rodem(
                dset, path, datasets, leading, incl_substruc, n_jets, n_csts, min_n_csts
            )
        elif "toptag" in path:
            self.high_data, self.node_data, self.mask, self.labels = load_toptag(
                dset, path, datasets, n_jets, n_csts, min_n_csts
            )
        self.n_classes = len(datasets)

        ## Check for Nan's (happens sometimes...)
        if np.isnan(self.high_data).any():
            raise ValueError("Detected NaNs in the jet data!")
        if np.isnan(self.node_data).any():
            raise ValueError("Detected NaNs in the constituent data!")

    def plot_fit_and_save_preprocess(self, path: Path, max_events: int = 10_000):
        """Fit the dictionary of sklearn scalers in the self.scalers and save.

        Graph networks should have a scaler inplace for each of their inputs:
        - edges, nodes, globals, condits

        Because of the augmentations, variable selection, and physical inputs
        done in this dataset the scalers have to be iteratively fit using samples
        drawn with the __getitem__ method.

        args:
            path: Where to save the scalers, usually the path to a network
        kwargs:
            max_events: Max number of events to sample to fit the scalers
        """
        print("Plotting data and fitting the preprocessing scalars")

        ## Create empty lists to hold the data used to fit the scalers
        num_csts = []
        all_data = {"edge": [], "node": [], "high": []}

        ## Cycle through the datasets, keeping only the data which is requested!
        take_every = int(len(self) / min(len(self), max_events))
        for i in np.arange(0, len(self), take_every):
            graph_tuple = self.__getitem__(i, do_preprocess=False, as_graph=False)
            edges, nodes, globs, _, mask, cntxt, _ = graph_tuple
            num_csts.append(np.sum(mask))
            all_data["edge"].append(edges)
            all_data["node"].append(nodes[mask])
            if self.high_in_graph == "glob":
                all_data["high"].append(globs)
            elif self.high_in_graph == "context":
                all_data["high"].append(cntxt)

        ## Combine the list of arrays into one single block
        plot_path = path / "train_dist"
        Path(plot_path).mkdir(parents=True, exist_ok=True)

        ## Make a histogram of the multiplicities
        plot_multi_hists(
            Path(plot_path, "num_csts"),
            [np.expand_dims(np.array(num_csts), 1)],
            ["inclusive"],
            ["num_csts"],
            bins=np.arange(0, self.n_nodes + 2),
        )

        ## For each data type: combine -> plot -> process -> plot again
        for key in all_data.keys():

            ## combine if there is any data to combine
            try:
                all_data[key] = np.vstack(all_data[key])
            except:
                continue

            ## plot
            if all_data[key].size:

                ## The labels for the plots come from the coords, but if substruc we
                ## need to manually add more
                coords = self.coordinates[key].copy()
                if self.incl_substruc and key == "high":
                    coords += [
                        "tau1",
                        "tau2",
                        "tau3",
                        "d12",
                        "d23",
                        "ECF2",
                        "ECF3",
                    ]

                plot_multi_hists(Path(plot_path, key), [all_data[key]], [key], coords)

                ## fit
                if self.scalers[key] is not None:
                    self.scalers[key].fit(all_data[key])

                    ## plot again
                    plot_multi_hists(
                        Path(plot_path, key + "_prcd"),
                        [self.scalers[key].transform(all_data[key])],
                        [key],
                        coords,
                    )

        ## Save the scalers
        dump(self.scalers, path / "scalers")

    def apply_preprocess(
        self,
        edges: np.ndarray,
        nodes: np.ndarray,
        high: np.ndarray,
        mask: np.ndarray,
    ) -> None:
        """Apply the pre-processing steps to a sample of data"""

        ## Apply the appropriate scaler to the data
        if self.scalers["edge"] is not None and edges.size:
            edges = self.scalers["edge"].transform(edges).astype("f")
        if self.scalers["node"] is not None and nodes.size:
            nodes[mask] = self.scalers["node"].transform(nodes[mask]).astype("f")
        if self.scalers["high"] is not None and high.size:
            high = self.scalers["high"].transform(high.reshape(1, -1)).astype("f")

        return edges, nodes, np.squeeze(high)

    def __getitem__(  # pylint ignore=arguments-differ
        self, idx: int, do_preprocess: bool = True, as_graph: bool = True
    ) -> Tuple[Graph, T.Tensor]:
        """Retrives a jet from the dataset and returns it as a graph object
        along with the class label

        args:
            idx: The index of the jet to pull from the dataset
        kwargs:
            do_preprocess: If the data should be pre-processed
            as_graph: If the returned
        """

        ## Load the particle constituent and high level jet information from the data
        nodes = self.node_data[idx].copy()
        mask = self.mask[idx].copy()
        high = self.high_data[idx].copy()
        label = self.labels[idx].copy()

        ## Apply all augmentations
        if self.augmentation_list and self.do_augment:
            nodes, high, mask = apply_augmentations(
                nodes, high, mask, self.augmentation_list, self.augmentation_prob
            )

        ## Build jet edges (will return empty if del_r is set to 0)
        edges, adjmat = build_jet_edges(
            nodes, mask, self.coordinates["edge"], self.del_r_edges
        )

        ## Apply boost pre-processing after the jet edges
        if self.boost_mopt != 0:
            nodes, high = boost_jet_mopt(nodes, high, self.boost_mopt)

        ## Convert to the specified selection of local variables and extract edges
        nodes, high = graph_coordinates(nodes, high, mask, self.coordinates)

        ## Apply the pre-processing scalers for each feature
        if do_preprocess:
            edges, nodes, high = self.apply_preprocess(edges, nodes, high, mask)

        ## How are we using the jet data
        globs = high if self.high_in_graph == "glob" else np.empty(0, dtype="f")
        cntxt = high if self.high_in_graph == "context" else np.empty(0, dtype="f")

        ## Return either the graph object or just the tuple
        if as_graph:
            return (
                Graph(
                    T.from_numpy(edges),
                    T.from_numpy(nodes),
                    T.from_numpy(globs),
                    T.from_numpy(adjmat),
                    T.from_numpy(mask),
                ),
                T.from_numpy(cntxt),
                T.tensor(label),
            )

        return edges, nodes, globs, adjmat, mask, cntxt, label


class AugJetData(JetData):
    """Returns the augmented and non augmented nodes only"""

    def __getitem__(  # pylint ignore=arguments-differ
        self,
        idx: int,
    ) -> Tuple[Graph, T.Tensor]:
        """Retrives a jet from the dataset and returns it as a graph object
        along with the class label

        args:
            idx: The index of the jet to pull from the dataset
        """

        ## Load the particle constituent and high level jet information from the data
        orig_nodes = self.node_data[idx].copy()
        orig_mask = self.mask[idx].copy()
        orig_high = self.high_data[idx].copy()

        ## Apply all augmentations
        aug_nodes, aug_high, aug_mask = apply_augmentations(
            orig_nodes.copy(),
            orig_high.copy(),
            orig_mask.copy(),
            self.augmentation_list,
            self.augmentation_prob,
        )

        ## Convert both point clouds to our selection of coordinates
        orig_nodes, _ = graph_coordinates(
            orig_nodes, orig_high, orig_mask, self.coordinates
        )
        aug_nodes, _ = graph_coordinates(
            aug_nodes, aug_high, aug_mask, self.coordinates
        )

        ## Ensure zero_padding is active
        orig_nodes[~orig_mask] = 0
        aug_nodes[~aug_mask] = 0

        ## Logsquash the mass of the augmented nodes
        aug_nodes[..., -1] = log_squash(aug_nodes[..., -1])

        return orig_nodes, aug_nodes
