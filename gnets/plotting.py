"""
Functions used for plotting and saving images
"""

from pathlib import Path
import numpy as np

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA

dflt_cycle = plt.rcParams["axes.prop_cycle"].by_key()["color"]


def plot_pc_classes(
    path: Path,
    nodes: np.ndarray,
    mask: np.ndarray,
    outputs: np.ndarray,
    labels: np.ndarray,
):
    """Plot the points clouds with their class predictions shown below

    args:
        path: The save location of the plot
        nodes: The attributes of the point cloud [batch, n_nodes, node_dim]
        outputs: The class probabilities as estimated by the network [batch, n_classes]
        labels: The real class index of the data [batch, 1]
    """

    ## The dimensions of the point clouds
    b_size = len(nodes)
    classes = np.arange(outputs.shape[-1])
    n_dim = nodes[0].shape[-1]

    ## Only works for 2, 3 dimensional point clouds
    if n_dim not in [2, 3]:
        return 0

    ## Create the figure (axes come later as they are either 2D or 3D)
    fig = plt.Figure(figsize=(4 * b_size, 4))

    ## Loop through each of the samples
    for i in range(b_size):

        ## Plot the point cloud
        if n_dim == 2:
            ax_pnts = fig.add_subplot(2, b_size, i + 1)
            ax_pnts.plot(*nodes[i][mask[i]].T, "o", color="red", label=str(labels[i]))
        if n_dim == 3:
            ax_pnts = fig.add_subplot(2, b_size, i + 1, projection="3d")
            ax_pnts.scatter(
                *nodes[i][mask[i]].T, "o", color="red", label=str(labels[i])
            )

        ## Plot the histogram
        ax_clss = fig.add_subplot(2, b_size, i + b_size + 1)
        ax_clss.bar(classes, outputs[i])
        ax_clss.set_ylim([0, 1])

        ## Edit the PC plot
        ax_pnts.legend()
        ax_pnts.xaxis.set_ticklabels([])
        ax_pnts.yaxis.set_ticklabels([])
        ax_pnts.xaxis.set_ticks_position("none")
        ax_pnts.yaxis.set_ticks_position("none")
        if n_dim == 2:
            ax_pnts.set_aspect("equal", adjustable="datalim")

    ## Tighten and save the image
    fig.tight_layout()
    fig.savefig(path.with_suffix(".png"))
    plt.close(fig)


def plot_latent_space(path, latents, labels=None, num_classes=None):
    """Create a scatter plot of the latent space
    - Will use PCA to project back to 2D for high dimensional plots
    - If labels are provided it will use for colours
    """

    ## Create the figure
    fig, axis = plt.subplots(1, 1, figsize=(5, 5))
    fig.suptitle("Latent Space")

    ## Reduce to 2 dimensions using PCA
    if latents.shape[-1] > 2:
        latents = PCA(2).fit_transform(latents)

    ## With no labels or classes provided then we just use a scatter
    if labels is None or num_classes is None:
        scttr = axis.scatter(*latents.T)

    ## With labels then we use a colored plot
    else:
        scttr = axis.scatter(
            *latents.T, c=labels, vmin=0, vmax=num_classes - 1, cmap="rainbow"
        )
        fig.colorbar(scttr)

    fig.tight_layout()
    fig.savefig(path.with_suffix(".png"))
    plt.close(fig)
