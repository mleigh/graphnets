from typing import Any
import numpy as np
import h5py
from pathlib import Path

def sigmoid(x) -> Any:
    return 1 / (1 + np.exp(-x))

#########################
tagger_dir = "/home/users/l/leighm/scratch/WhitePaperExports/WithEdges/transformer_ttbar_999"
data_dir = "/srv/beegfs/scratch/groups/rodem/anomalous_jets/virtual_data/"
out_dir = "/srv/beegfs/scratch/groups/rodem/anomalous_jets/decor_data"
jet_vals = ["pt", "eta", "phi", "mass"]
score_name = "score"
data_names = [
    # "QCD_jj_pt_450_1200_test.h5",
    # "WZ_allhad_pt_450_1200_test.h5",
    "ttbar_allhad_pt_450_1200_test.h5",
]
#########################


# Loop through all the data
for data_file in data_names:

    # Load the jet data and the scores
    with h5py.File(Path(data_dir, data_file), "r") as f:
        jet_data = f["objects/jets/jet1_obs"][:, :4].astype("float32")
    with h5py.File(Path(tagger_dir, data_file), "r") as f:
        scores = sigmoid(f[score_name][:].astype("float32"))
    assert len(jet_data) == len(scores)

    # Select only the jet inputs that were requested (does not copy data)
    idxes = {"pt": 0, "eta": 1, "phi": 2, "mass": 3}
    idxes = np.array([idxes[k] for k in jet_vals])
    selected_data = jet_data[..., idxes]

    # Combine with the scores in a named array
    rec_arr = np.rec.fromarrays(
        np.hstack([selected_data, scores]).T,
        names = jet_vals + ["score"]
    )

    # Save the output to a new file
    with h5py.File(Path(out_dir, Path(tagger_dir).stem, data_file), "w") as f:
        f.create_dataset("data", data=rec_arr)
        f.close()