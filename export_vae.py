"""
For creating ROC curves when evaluating jet classifier performance
"""

from functools import reduce
import h5py
from tqdm import tqdm
from joblib import load
from pathlib import Path
from dotmap import DotMap

import torch as T
import torch.nn as nn
from torch.utils.data import DataLoader

from mattstools.loss import masked_dist_loss, kld_to_norm, EnergyMovers
from mattstools.torch_utils import (
    sel_device,
    count_parameters,
    move_dev,
    to_np,
    get_loss_fn,
)
from mattstools.utils import load_yaml_files, save_yaml_files
from mattstools.gnets.graphs import gcoll

from gnets.datasets import JetData

DATA_NAMES = {
    "QCD": "QCD_jj_pt_450_1200_",
    "ttbar": "ttbar_allhad_pt_450_1200_",
    "WZ": "WZ_allhad_pt_450_1200_",
    "H2": "H2tbtb_1800_HC_400_",
}

T.manual_seed(42)
T.set_grad_enabled(False)


def main():

    ###############################################
    b_size = 512
    n_wrkrs = 8
    device = sel_device("gpu")

    target_path = "/home/users/l/leighm/scratch/WhitePaperExports/Unsupervised/"
    net_folder = (
        "/home/users/l/leighm/scratch/Saved_Networks/JetTag/WhitePaper/Unsupervised/"
    )
    net_names = [
        "TransformerVAE_EMD_5e5",
    ]
    file_name = "best"
    procs = ["H2", "WZ", "QCD", "ttbar"]
    # procs.reverse()
    # net_names.reverse()
    ###############################################

    ## Define the permuation invariant loss functions to be used as anomaly scores
    # weighted_sink_loss_fn = get_loss_fn("sinkhorn")
    sink_loss_fn = get_loss_fn("sinkhorn")
    emd_loss_fn = get_loss_fn("energymovers")

    old_data_conf = {}

    ## Cycle through the processes
    for proc in procs:

        ## Cycle through each of the networks
        for net_nm in net_names:
            print()
            print(net_nm)

            ## Load the network
            model_folder = Path(net_folder, net_nm)
            network = T.load(model_folder / file_name, map_location=device)
            network.set_device(device)
            network.eval()

            ## Load the data and network config
            data_conf = DotMap(load_yaml_files(model_folder / "config/data.yaml"))
            net_conf = DotMap(load_yaml_files(model_folder / "config/net.yaml"))

            ## Create the output name and directory
            out_nm = "_".join([net_conf.base_kwargs.name, str(net_conf.lat_dim)])
            outpath = Path(target_path) / out_nm
            outpath.mkdir(parents=True, exist_ok=True)
            print(f"-> {out_nm}")

            ## Save the config file
            save_yaml_files(
                outpath,
                "config",
                {
                    "model_name": f"QCD_{net_nm}",
                    "creator_names": "Matthew Leigh",
                    "datasets": {
                        "c1": {DATA_NAMES["QCD"] + "train.h5": data_conf.n_jets},
                    },
                    "n_csts": data_conf.n_csts,
                    "learn_params": count_parameters(network),
                    "leading": data_conf.leading,
                    "incl_substruc": data_conf.incl_substruc,
                    "coordinates": data_conf.coordinates,
                    "scores": ["sinkhorn", "energymovers", "kld"],
                },
            )

            ## Modify the config for the specific test set
            data_conf.datasets = {"c1": [proc]}
            data_conf.n_jets = -1
            data_conf.min_n_csts = 0

            ## Load new data if needed, otherwise data is reused
            if data_conf != old_data_conf:
                dataset = JetData(dset="test", **data_conf)
                dataset.scalers = load(network.full_name / "scalers")
                old_data_conf = data_conf

            ## Create a simple dataloader to allow us to batch the data quickly
            loader = DataLoader(
                dataset,
                batch_size=b_size,
                collate_fn=gcoll,
                drop_last=False,
                shuffle=False,
                num_workers=n_wrkrs,
            )

            ## Create running stores of the losses
            weighted_sink_losses = []
            sink_losses = []
            emd_losses = []
            kld_losses = []

            ## Iterate through the dataloader
            for sample in tqdm(loader, desc="eval", ncols=80):
                graphs, _ = move_dev(sample, device)
                rec_nodes, _, means, lstds = network(graphs)

                mask = graphs.mask
                org_nodes = graphs.nodes
                rec_nodes = rec_nodes

                ## Calculate the sinkhorn weighted by pt
                # weighted_sink_losses.append(
                #     masked_dist_loss(
                #         weighted_sink_loss_fn,
                #         T.from_numpy(org_nodes[..., :2]),
                #         T.from_numpy(org_nodes[..., 2]),
                #         T.from_numpy(rec_nodes[..., :2]),
                #         T.from_numpy(rec_nodes[..., 2]),
                #         reduce="none",
                #     )
                # )

                ## Calculate the KLD losses
                kld_losses.append(kld_to_norm(means, lstds, reduce="dim_mean"))

                ## Calculate the normal sinkhorn loss
                sink_losses.append(
                    masked_dist_loss(
                        sink_loss_fn,
                        org_nodes,
                        mask,
                        rec_nodes,
                        mask,
                        reduce="none",
                    )
                )

                ## Calculate the emd loss
                emd_losses.append(
                    masked_dist_loss(
                        emd_loss_fn,
                        org_nodes,
                        mask,
                        rec_nodes,
                        mask,
                        reduce="none",
                    )
                )

            # weighted_sink_losses = to_np(T.cat(weighted_sink_losses))
            sink_losses = to_np(T.cat(sink_losses))
            emd_losses = to_np(T.cat(emd_losses))
            kld_losses = to_np(T.cat(kld_losses))

            ## Save the losses to file
            hf = h5py.File(outpath / (DATA_NAMES[proc] + "test.h5"), "w")
            # hf.create_dataset("weighted_sinkhorn", data=weighted_sink_losses)
            # hf.create_dataset("sinkhorn", data=sink_losses)
            hf.create_dataset("energymovers", data=emd_losses)
            hf.create_dataset("kld", data=kld_losses)
            hf.close()


if __name__ == "__main__":
    main()
