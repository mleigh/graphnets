"""
For creating ROC curves when evaluating jet classifier performance
"""

from pathlib import Path
import h5py
from tqdm import tqdm
from joblib import load
import numpy as np

from dotmap import DotMap

import torch as T
import torch.nn as nn
from torch.utils.data import DataLoader

from mattstools.torch_utils import sel_device, count_parameters, move_dev, to_np
from mattstools.utils import load_yaml_files, save_yaml_files
from mattstools.gnets.graphs import gcoll

from gnets.datasets import JetData

DATA_NAMES = {
    "QCD": "QCD_jj_pt_550_650_",
    "ttbar": "ttbar_allhad_pt_550_650_",
    "WZ": "WZ_jjnunu_pT_550_650_",
    # "H2": "H2tbtb_1800_HC_400_",
}


T.manual_seed(42)
T.set_grad_enabled(False)


def process_dataset(loader: DataLoader, network: nn.Module) -> np.ndarray:
    """Return a numpy array of the network's output using its forward method"""
    preds = []
    for batch in tqdm(loader, desc="eval", ncols=80):
        graphs, ctxt, _ = move_dev(batch, network.device)
        preds.append(network(graphs, ctxt))
    return to_np(T.cat(preds))


def main():

    ###############################################
    b_size = 512
    n_wrkrs = 4
    n_test_augments = 0
    device = sel_device("gpu")

    target_path = "/srv/beegfs/scratch/groups/rodem/anomalous_jets/exported_taggers/"
    net_folder = "/home/users/l/leighm/scratch/Saved_Networks/JetTag/Reduced/"
    net_names = [
        "Transformer2_Restricted",
        # "GraphNet_Restricted",
    ]
    file_name = "best"
    procs = ["WZ"]
    ###############################################

    old_data_conf = {}

    ## Cycle through the processes
    for proc in procs:

        ## Cycle through each of the networks
        for net_nm in net_names:
            print()
            print(net_nm)

            ## Load the network
            model_folder = Path(net_folder, net_nm)
            network = T.load(model_folder / file_name, map_location=device)
            network.set_device(device)
            network.eval()

            ## Load the data and network config
            data_conf = DotMap(load_yaml_files(model_folder / "config/data.yaml"))
            net_conf = DotMap(load_yaml_files(model_folder / "config/net.yaml"))

            ## Create the output name and directory
            out_nm = "_".join(
                [
                    net_conf.base_kwargs.net_type,
                    "restricted",
                ]
            )
            outpath = Path(target_path) / out_nm
            outpath.mkdir(parents=True, exist_ok=True)
            print(f"-> {out_nm}")

            ## Save the config file
            save_yaml_files(
                outpath,
                "config",
                {
                    "model_name": net_nm
                    + f"_{data_conf.datasets.c1[0]}vs{data_conf.datasets.c2[0]}_classifier",
                    "creator_names": "Matthew Leigh",
                    "datasets": {
                        "c1": {
                            DATA_NAMES[data_conf.datasets.c1[0]]
                            + "train.h5": data_conf.n_jets
                        },
                        "c2": {
                            DATA_NAMES[data_conf.datasets.c2[0]]
                            + "train.h5": data_conf.n_jets
                        },
                    },
                    "n_csts": data_conf.n_csts,
                    "learn_params": count_parameters(network),
                    "leading": data_conf.leading,
                    "incl_substruc": data_conf.incl_substruc,
                    "coordinates": data_conf.coordinates,
                    "scores": ["score"],
                },
            )

            ## Modify the config for the specific test set
            data_conf.datasets = {"c1": [proc]}
            data_conf.n_jets = -1
            data_conf.min_n_csts = 0

            ## Load new data if needed, otherwise data is reused
            if data_conf != old_data_conf:
                dataset = JetData(dset="test", **data_conf)
                dataset.scalers = load(model_folder / "scalers")
                old_data_conf = data_conf.copy()

            ## Create a simple dataloader to allow us to batch the data quickly
            loader = DataLoader(
                dataset,
                batch_size=b_size,
                collate_fn=gcoll,
                drop_last=False,
                shuffle=False,
                num_workers=n_wrkrs,
            )

            ## Get the outputs of the network multiple times if test augmentation is on
            preds = process_dataset(loader, network)
            if dataset.do_augment and len(dataset.augmentation_list) != 0:
                for _ in range(1, n_test_augments):
                    preds += process_dataset(loader, network)
                preds /= n_test_augments

            file = h5py.File(outpath / (DATA_NAMES[proc] + "test.h5"), "w")
            file.create_dataset("score", data=preds)
            file.close()


if __name__ == "__main__":
    main()
